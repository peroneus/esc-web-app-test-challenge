import React, {Component} from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import TrainingPath from '../../components/TrainingPath';
import TrainingRequirements from '../../components/TrainingRequirements';
import TrainingStages from '../../components/TrainingStages';

import { actions } from '../../../../reducers/embryos_training_reducer';

import './styles.scss';

class Dashboard extends Component {

	componentDidMount() {
		this.requestUserProgress();
	}

	componentDidUpdate() {

		if(!this.userProgressRequested) {
			this.requestUserProgress();
		}

	}

	requestUserProgress() {

		const nextLevel = (this.props.nextLevel !== undefined) ? this.props.nextLevel : undefined;
		const currentVisitedLevel = (this.props.currentVisitedLevel === undefined || this.props.currentVisitedLevel === "") ? nextLevel : this.props.currentVisitedLevel;
		const levelToLoad = (nextLevel !== currentVisitedLevel) ? currentVisitedLevel : nextLevel;

		this.props.dispatch(actions.requestEmbryosTrainingUserProgress(levelToLoad));

		this.userProgressRequested = true;

	}

	isTestLocked() {

		let result = true;
		const userProgress = this.props.userProgress;

		if(userProgress !== undefined) {

			if(userProgress.currentTrainedEmbryos < userProgress.totalEmbryos) {

					result = true;

			} else {

				if(userProgress.accuracyPercentage >= userProgress.minAccuracyPercentage)
					result = false;

			}

		}

		return result;

	}


	render() {

		const userProgress = this.props.userProgress;
		const testLocked = this.isTestLocked();
		const currentVisitedLevel = this.props.currentVisitedLevel;

		const animateFirstLevelUp = this.props.animateFirstLevelUp;
		const firstLevelUpAnimationSeen = this.props.firstLevelUpAnimationSeen;
		const animateSecondLevelUp = this.props.animateSecondLevelUp
		const secondLevelUpAnimationSeen = this.props.secondLevelUpAnimationSeen;

		return (Object.keys(userProgress).length > 0) ? (

			<div className="dashboard-container">

				<TrainingPath

					currentLevel={userProgress.currentLevel}
					currentVisitedLevel={currentVisitedLevel}
					nextLevel={userProgress.nextLevel !== null ? userProgress.nextLevel : 'expert'}

					currentTrainedEmbryos={userProgress.currentTrainedEmbryos}
					totalEmbryos={userProgress.totalEmbryos}

					animateFirstLevelUp={animateFirstLevelUp}
					firstLevelUpAnimationSeen={firstLevelUpAnimationSeen}
					animateSecondLevelUp={animateSecondLevelUp}
					secondLevelUpAnimationSeen={secondLevelUpAnimationSeen}

				/>

				<div className="row req-stag-wrapper">
					<div className="col">
						<div className="container">

						<h3 className="main">Complete training requirements to take the test. After passing the test, you will become an expert.</h3>

							<div className="row">

								<div className="col-lg-7">

									<TrainingRequirements

										minAccuracyPercentage={userProgress.minAccuracyPercentage}
										accuracyPercentage={userProgress.accuracyPercentage}

										currentTrainedEmbryos={userProgress.currentTrainedEmbryos}
										totalEmbryos={userProgress.totalEmbryos}
									/>

								</div>

								<div className="col-lg-5">

									<TrainingStages

										currentLevel={userProgress.currentLevel}
										currentVisitedLevel={currentVisitedLevel}
										nextLevel={userProgress.nextLevel}

										testLocked={testLocked}

									/>

								</div>

							</div>
						</div>
					</div>

				</div>

			</div>

		) : null;

	}

}

const mapStateToProps = (state) => {

	return {

		userProgress: state.embryosTrainingReducer.data,
		currentVisitedLevel: state.embryosTrainingReducer.currentVisitedLevel,

		animateFirstLevelUp: state.embryosTrainingReducer.animateFirstLevelUp,
		firstLevelUpAnimationSeen: state.embryosTrainingReducer.firstLevelUpAnimationSeen,
		animateSecondLevelUp: state.embryosTrainingReducer.animateSecondLevelUp,
		secondLevelUpAnimationSeen: state.embryosTrainingReducer.secondLevelUpAnimationSeen,

	};

}

Dashboard.propTypes = {

	userProgress: PropTypes.object,
	currentVisitedLevel: PropTypes.string,

	dispatch: PropTypes.func.isRequired,

	animateFirstLevelUp: PropTypes.bool,
	firstLevelUpAnimationSeen: PropTypes.bool,
	animateSecondLevelUp: PropTypes.bool,
	secondLevelUpAnimationSeen: PropTypes.bool,

};

export default connect(mapStateToProps)(Dashboard);

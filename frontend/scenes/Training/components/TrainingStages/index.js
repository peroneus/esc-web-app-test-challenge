import React, {Component} from 'react';
import PropTypes from 'prop-types';
import { NavLink } from 'react-router-dom';

import './styles.scss';

import LockCircleIcon from '../../../../components/LockCircleIcon';
import DoneTestCircleIcon from '../../../../components/DoneTestCircleIcon';

class TrainingStages extends Component {

	constructor(props) {

		super(props);
		this.onContinueTrainingClick = this.onContinueTrainingClick.bind(this);

	}

	onContinueTrainingClick() {

	}

	isTestAlreadyPassed() {

		let result = false;
		const currentLevel = this.props.currentLevel;
		const currentVisitedLevel = this.props.currentVisitedLevel;

		if(currentLevel === "beginner") {

			if(currentVisitedLevel === "beginner")
				result = true;

		} else if(currentLevel === "intermediate") {

			if(currentVisitedLevel === "beginner")
				result = true;
			else if(currentVisitedLevel === "intermediate")
				result = true;

		} else if(currentLevel === "expert") {
			result = true;
		}

		return result;

	}

	render() {

		const testLocked = this.props.testLocked;
		const nextLevel = this.props.nextLevel !== null ? this.props.nextLevel : 'expert';
		const levelToLoad = this.props.currentVisitedLevel === undefined ? nextLevel : this.props.currentVisitedLevel;

		let testSectionClass = "";
		let overlaySection = null;

		if (this.isTestAlreadyPassed()) {

			testSectionClass = "test-section inner locked";
			overlaySection = <div className="locked-wrapper"><DoneTestCircleIcon /><p className="orange">You have completed this level</p></div>;

		} else {

			if(testLocked) {

				testSectionClass = "test-section inner locked";
				overlaySection = <div className="locked-wrapper"><LockCircleIcon /><p>Finish training to unlock test</p></div>;

			} else {

				testSectionClass = "test-section inner";
				overlaySection = null;

			}

		}

		const testLocation = {
			pathname: "/training/embryos_test",
			state: { levelToLoad: levelToLoad}
		};


		return (

			<div className="training-stages-questions-container">

				<div className="inner first">

					<p>Stages training</p>
					<span className="training-icon active" />
					<NavLink className="btn-embryo continue-training-button"
							to="/training/embryo_stage_selection"
							activeClassName="active">Start training</NavLink>

				</div>

				<div className={testSectionClass}>

					<p>Test</p>
					<span className="test-icon active" />

					<NavLink
						className="btn-embryo take-the-test-button"
						to={testLocation}
						activeClassName="active">Take the test</NavLink>

				</div>

				{overlaySection}

			</div>
		);

	}

}

TrainingStages.propTypes = {

	currentLevel: PropTypes.string,
	currentVisitedLevel: PropTypes.string,
	nextLevel: PropTypes.string,

	testLocked: PropTypes.bool,

};

export default TrainingStages;

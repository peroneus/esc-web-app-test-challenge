import React, {Component} from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import { goToTop } from 'react-scrollable-anchor';

import StagesList from '../../../../components/StagesList';
import ArrowRight from '../../../../components/ArrowRight';

import { actions } from '../../../../reducers/embryos_training_reducer';

import './styles.scss';


class TrainingQuestionOptions extends Component {

	constructor(props) {

		super(props);
		this.onConfirmSelectionClick = this.onConfirmSelectionClick.bind(this);

	}

	getStagesList() {

		let stages = [];
		const options = this.props.options;

		options.forEach(function(option) {
			stages.push(option.value);
		});

		return stages;

	}

	onConfirmSelectionClick(e) {

		const currentTrainingQuestion = this.props.currentTrainingQuestion;
		const currentTrainingQuestionResult = this.props.currentTrainingQuestionResult;
		const currentSelectedStage = this.props.currentTrainingQuestionSelectedOption;

		const levelToLoad = this.props.nextLevel;

		if(this.callToGiveAnswer === true)
			return e.preventDefault();

		if(Object.keys(currentSelectedStage).length > 0) {

			// Action: Confirm Selection
			if(currentTrainingQuestionResult === undefined || Object.keys(currentTrainingQuestionResult).length === 0) {

				this.confirmSelectionClicked = true;

				const id = currentTrainingQuestion.id;
				const answer = currentSelectedStage.value;

				this.callToGiveAnswer = true;
				this.props.dispatch(actions.requestEmbryosTrainingQuestionResult(id, answer));

			}

			// Action: Go to the next question
			else {

				this.props.dispatch(actions.requestEmbryosTrainingQuestion(levelToLoad));
				goToTop();

			}

		}

	}

	getCurrentSelectedOption(type, testQuestionIndex) {

		let result = null;

		if(type === "training") {
			result = this.props.currentTrainingQuestionSelectedOption;

		} else if(type === "test") {
			result = this.props.currentTrainingTestSelectedOptions[testQuestionIndex];

		}


		return result;

	}

	getTitleByLevel(level, correct) {

		let result = "";
		const currentTrainingQuestionResult = this.props.currentTrainingQuestionResult;
		const currentTrainingTestResult = this.props.currentTrainingTestResult;

		// Answered
		if(Object.keys(currentTrainingQuestionResult).length > 0 || Object.keys(currentTrainingTestResult).length > 0) {

			if(correct) {
				result = <h2><span className='correct'>Correct!</span></h2>;
			} else {
				result = <h2><span className='incorrect'>Incorrect!</span></h2>;
			}

		}

		// No answered yet
		else {

			if(level === "beginner") {
				result = <h2><span className='active-color'>Select </span>correct stage of the embryo above</h2>;
			} else if(level === "intermediate") {
				result = <h2><span className='active-color'>Select </span>correct quality of the embryo above</h2>;
			} else if(level === "expert") {
				result = <h2><span className='active-color'>Select </span>correct event timing of the embryo above</h2>;
			}

		}

		return result;

	}

	render() {

		this.callToGiveAnswer = false;

		const levelToLoad = this.props.nextLevel;

		const type = this.props.type;
		const testQuestionId = this.props.testQuestionId;
		const testQuestionIndex = this.props.testQuestionIndex;

		const options = this.props.options;
		const stagesList = this.getStagesList();

		const currentSelectedOption = this.getCurrentSelectedOption(type, testQuestionIndex);
		let currentSelectedOptionValue = null;

		if(currentSelectedOption !== undefined) {
			currentSelectedOptionValue = currentSelectedOption.value;
		}

		// For Test: Question Result

		const currentTrainingTestResult = this.props.currentTrainingTestResult;
		let successTestQuestionResult = null;

		if(Object.keys(currentTrainingTestResult).length > 0) {
			successTestQuestionResult = currentTrainingTestResult.answeredQuestions[testQuestionIndex].success;
		}

		// For Training: Question Result

		const currentTrainingQuestionResult = this.props.currentTrainingQuestionResult;

		let isCorrectSelectedOption = null;
		let confirmSelectionButtonText = "";
		let questionOptionsContainerClass = "";

		if (Object.keys(currentTrainingQuestionResult).length > 0 || Object.keys(currentTrainingTestResult).length > 0) {

			isCorrectSelectedOption = !!currentTrainingQuestionResult.success;
			questionOptionsContainerClass = "row training-question-options-container answered";
			confirmSelectionButtonText = "Next question";

		} else {

			questionOptionsContainerClass = "row training-question-options-container";
			confirmSelectionButtonText = "Confirm selection";

		}


		const confirmSelectionButtonClass = (this.props.type === "training" && Object.keys(currentSelectedOption).length > 0) ? "btn btn-orange continue" : "btn btn-orange continue disabled";
		const confirmSelectionButton = (this.props.type === "training") ? <a className={confirmSelectionButtonClass} onClick={this.onConfirmSelectionClick}>{confirmSelectionButtonText}<ArrowRight direction="right"/></a>
										: null;

		// Answered Question Flag

		let answered = false;

		if(type === "training")
			answered = isCorrectSelectedOption;
		else if(type === "test")
			answered = successTestQuestionResult;

		return (

			<div className={questionOptionsContainerClass}>

				<div className="col">

						{this.getTitleByLevel(levelToLoad, answered)}

						<StagesList

							type={type}

							stagesList={stagesList}
							options={options}

							currentSelectedStage={currentSelectedOption}
							currentSelectedStageValue={currentSelectedOptionValue}

							isCorrectSelectedOption={isCorrectSelectedOption}
							currentTrainingQuestionResult={currentTrainingQuestionResult}

							currentTrainingTestResult={currentTrainingTestResult}

							testQuestionId={testQuestionId}
							testQuestionIndex={testQuestionIndex}

						/>

						{confirmSelectionButton}

				</div>

			</div>

		);

	}

}


const mapStateToProps = (state) => {

	return {

		currentTrainingQuestionResult: state.embryosTrainingReducer.currentTrainingQuestionResult,
		currentTrainingTestResult: state.embryosTrainingReducer.currentTrainingTestResult,

		currentTrainingQuestionSelectedOption: state.embryosTrainingReducer.currentTrainingQuestionSelectedOption,
		currentTrainingTestSelectedOptions: state.embryosTrainingReducer.currentTrainingTestSelectedOptions

	};

};

TrainingQuestionOptions.propTypes = {

	type: PropTypes.string,
	options: PropTypes.array,

	dispatch: PropTypes.func.isRequired,

	nextLevel: PropTypes.string,

	currentTrainingQuestion: PropTypes.object,

	currentTrainingQuestionResult: PropTypes.object,
	currentTrainingQuestionSelectedOption: PropTypes.object,

	currentTrainingTestResult: PropTypes.object,
	currentTrainingTestSelectedOptions: PropTypes.array,

	testQuestionId: PropTypes.number,
	testQuestionIndex: PropTypes.number,

};

export default connect(mapStateToProps)(TrainingQuestionOptions);

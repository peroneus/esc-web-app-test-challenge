import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import { actions } from '../../../../reducers/embryos_learning_reducer';

import './styles.scss';

import introImageFirst from '../../icon-embryoviability.jpg';
import introImageSecond from '../../icon-geneticconstitution.svg';
import arrowsCati from '../../icon-catigradingsystem.svg';

class Introduction extends Component {

	componentDidMount() {
		this.props.dispatch(actions.visitEmbryosLearning('introduction'));
	}

	componentWillUnmount() {
		this.props.dispatch(actions.leaveEmbryosLearning('introduction'));
	}

	render() {

		return (
			<section className="learning-main" id="introduction">
				<header>
					<div className="designation-box">
						<div className="designation-num active">D5</div>
					</div>

					<div className="main-desc">
						<p><b>D5 Evaluation of embryos</b> considers embryo's <b>viability</b> and <b>genetic constitution</b>. Those are the two, relatively independent, properties of early embryos that are considered in <b>CATI grading system</b>.</p>
					</div>
				</header>
				<section id="main-properties">
					<h2>Two main properties of early embryos</h2>
					<div className="content-box">
						<div className="inside">
							<img height="200" width="200" src={introImageFirst} alt="" />
							<h3>Viability</h3>
							<p>Viability of early embryo corresponds to Implantation potential (IP) and is expressed by its <b>ability to develop to blastocyst stage</b>. IP calculation is based on the <b>embryo's chance for implantation</b> given ideal in-vivo conditions.</p>

							<div className="half-box">
								<h3 className="blue-list">Implantation potentional (IP)</h3>
								<p>Viability of embryo.</p>
							</div>

							<div className="number-bg number-bg-blurred">1</div>
						</div>
						<div className="number-bg">1</div>
					</div>
					<div className="content-box">
						<div className="inside">
							<img height="200" width="200" src={introImageSecond} alt="" />
							<h3>Genetic constitution</h3>
							<p>Chromosomal aneuploidies resulting from malsegregations after abnormal cell cleavages. <b>80% of mitotic errors are time-lapse recognizable.</b></p>

							<div className="half-box">
								<h3 className="blue-list">Aneuploidy (AR)</h3>
								<p>Risk of developmental arrest, implantation failure and early misscarriages.</p>
							</div>

							<div className="half-box">
								<h3 className="blue-list">Mosaicism risk (MR)</h3>
								<p>Risk of misdiagnosis after aneuploidy screening (PGS) .</p>
							</div>

							<div className="number-bg number-bg-blurred">2</div>
						</div>
						<div className="number-bg">2</div>
					</div>
					<div className="inside" id="cati-grading">
						<h2>CATI grading system</h2>
						<img src={arrowsCati} alt="From 1AA to 8DA" />
						<p>CATI grading system used in ESCAPE ecosystem is represented by clusters consisting of 1 number and two letters.</p>
					</div>
				</section>
			</section>
		);

	}

}


Introduction.propTypes = {
	dispatch: PropTypes.func.isRequired,
};


const mapStateToProps = () => ({});


export default connect(mapStateToProps)(Introduction);

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import EmbryoDetail from '../../../../components/EmbryoDetail';
import StagesList from '../../../../components/StagesList';

import { actions } from '../../../../reducers/embryos_learning_reducer';

import './styles.scss';

class StagesOfDevelopment extends Component {

	componentDidMount() {
		this.props.dispatch(actions.visitEmbryosLearning('stagesOfDevelopment'));
	}

	componentWillUnmount() {
		this.props.dispatch(actions.leaveEmbryosLearning('stagesOfDevelopment'));
	}

	onStageItemClick(stage) {

		const clustersByStage = this.props.clustersByStage;
		const stageObject = clustersByStage[stage];

		this.props.dispatch(actions.uiSelectStage(stageObject));

	}

	getStagesList() {

		let stages = [];
		const clustersByStage = this.props.clustersByStage;

		Object.keys(clustersByStage).forEach(function (key) {
			stages.push(key);

		});

		return stages;

	}

	getStageValueFromCurrentDevelopmentStageObject() {

		return this.props.currentDevelopmentStage.classification;

	}

	moveReferenceLine = (value) => {
		this.props.dispatch(actions.uiMoveCurrentStageDevelopmentReferenceLine(value));
	};

	render() {

		// Current Development Stage

		const currentDevelopmentStage = this.props.currentDevelopmentStage;
		const currentDevelopmentExists = currentDevelopmentStage !== undefined && Object.keys(currentDevelopmentStage).length !== 0;

		// Stages Value List

		const stagesList = this.getStagesList();

		// Current Selected Stage Value

		let currentSelectedStageValue = null;
		if(stagesList.length > 0) {

			currentSelectedStageValue = (currentDevelopmentStage !== undefined) && (Object.keys(currentDevelopmentStage).length > 0) ?
									this.getStageValueFromCurrentDevelopmentStageObject() :
									stagesList[0];

		}

		// Current Development Stage: Divisions


		return (currentDevelopmentExists) ? (

			<section className="learning-main" id="stages-of-development">

				{/* Header */}

				<header>

					<div className="designation-box">
						<div className="designation-num active">{currentSelectedStageValue}</div>
						<div className="designation-num">{currentSelectedStageValue < 7 ? 'A' : 'D'}</div>
						<div className="designation-num">{currentSelectedStageValue < 7 ? 'A' : '\u00a0'}</div>
					</div>

					<div className="main-desc">
						<p>Number, which is in <b>the first position of the CATI grading system</b>, represents a degree of <b>embryo's expansion</b> of blastocyst cavity and its progress in hatching out of the zona pellucida on the scale from 1 to 8.</p>
					</div>

				</header>


				{/* Section Menu  */}

				<section id="main-properties">

					<h2>Choose stage</h2>

					<div className="inside">

						{/* List of Stages */}

						<div className="designation-box">

							<StagesList
								type="learning_stages"
								clustersByStage={this.props.clustersByStage}
								stagesList={stagesList}
								currentSelectedStageValue={currentSelectedStageValue}
							/>

						</div>


					{/* Chosen Stage */}

						<div className="designation-chosen">
							<div className="designation-num">
								<span className="active-color">{currentSelectedStageValue}</span>
							</div>
						</div>

						<h3>
							<span className="active-color">{currentDevelopmentStage.name}</span>
							<div className="desc">{currentDevelopmentStage.shortName}</div>
						</h3>

						<p>{currentDevelopmentStage.description}</p>

					</div>

					{currentDevelopmentStage.embryo && <div className="learning-embryo-graph-container">

						<EmbryoDetail
							embryo={currentDevelopmentStage.embryo}
							current={this.props.currentDevelopmentStagePlayPosition}
							onReferenceLineChange={this.moveReferenceLine}
						/>

					</div>}

				</section>

			</section>

		) : null;

	}

}

const mapStateToProps = (state) => {

	return {

		clustersByStage: state.embryosLearningReducer.data.clustersByStage,

		currentDevelopmentStage: state.embryosLearningReducer.currentDevelopmentStage,
		currentDevelopmentStagePlayPosition: state.embryosLearningReducer.currentDevelopmentStagePlayPosition

	};

};

StagesOfDevelopment.propTypes = {

	clustersByStage: PropTypes.object,

	currentDevelopmentStage: PropTypes.object,
	currentDevelopmentStagePlayPosition: PropTypes.number,

	dispatch: PropTypes.func.isRequired

};

export default connect(mapStateToProps)(StagesOfDevelopment);

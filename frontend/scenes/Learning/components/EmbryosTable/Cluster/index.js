import React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import PropTypes from 'prop-types';
import { actions } from '../../../../../reducers/embryos_library_reducer';


class Cluster extends React.Component
{

	redirectToLibraryHandler = () => {
		if (this.props.clickAble) {
			this.props.dispatch(actions.uiResetEmbryoFilter(this.props.developmentStageName, this.props.cluster.classification));
			window.scrollTo(0, 0);
			this.props.history.push('/embryo_library');
		}
	};

	render() {
		return (
			<div onClick={this.redirectToLibraryHandler} className={`embryo${this.props.className ? ` ${this.props.className}` : ''}`}>
				{this.props.embryo && <div className="img"><img src={this.props.embryo.last_image_url} alt={`Cluster ${this.props.cluster.classification} embryo`}/></div>}
				<div className="text">{this.props.cluster.classification}</div>

				<div className="pseudo-tooltip">
					{this.props.embryo && <img src={this.props.embryo.last_image_url} alt={`Cluster ${this.props.cluster.classification} embryo`}/>}
					<div className={this.props.tooltipClassName}>
						{this.props.implantationChance ? (
							<p>Implantation chance: {this.props.cluster.implantationChance}%</p>
						) : (
							<p className="cluster-name">{this.props.cluster.classification}</p>
						)}
					</div>
				</div>
			</div>
		)
	}

}

Cluster.propTypes = {
	className: PropTypes.string,
	tooltipClassName: PropTypes.string,
	implantationChance: PropTypes.bool,
	developmentStageName: PropTypes.string.isRequired,
	cluster: PropTypes.object.isRequired,
	embryo: PropTypes.object.isRequired,
	dispatch: PropTypes.func.isRequired
};

Cluster.defaultProps = {
	clickAble: true,
};

export default withRouter(connect()(Cluster));

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

const QUALITY_COLUMNS = ['a', 'b', 'c', 'd'];
const EMBRYO_WIDTH = 60;

class EmbryosTableByImplantanceChance extends Component {

	getStatusClassName(status) {
		switch (status) {
			case 'Normal': return 'normal';
			case 'Delayed': return 'delayed';
			case 'Collapses': return 'collapses';
			case 'Low cell count': return 'low';
			case 'Degenerated':
			case 'Arrested':
			case 'Unfertilized': return 'degenerated';
			default: return '';
		}
	}

	calculateEmbryoPositionX(columnIndex, implantanceChance) {
		const percentage = (1 - implantanceChance / 70.5) * 100;
		const embryoCorrection = -percentage * EMBRYO_WIDTH / 100;
		const borderCorrection = percentage * (QUALITY_COLUMNS.length - 8) / 100 - columnIndex;
		return `calc(${percentage * QUALITY_COLUMNS.length - 100 * columnIndex}% + ${embryoCorrection + borderCorrection}px)`;
	}

	renderEmbryos(stageKey) {
		const clustersByStage = this.props.clustersByStage;
		const stage = clustersByStage && clustersByStage[stageKey];

		if (!stage) {
			return null;
		}

		return QUALITY_COLUMNS.map((column, columnIndex) => {
			const classification = `${stage.classification}${column}`;
			const quality = stage.clustersByQuality && stage.clustersByQuality[classification];

			if (!quality) {
				return <td key={column} />;
			}

			const category = this.props.clusterCategories && this.props.clusterCategories[quality.category];
			const status = category && category.title;
			const className = status && this.getStatusClassName(status);

			const clusters = quality.clustersByEvents || {};
			const tooltipClassName = `impl-chance${className ? ` ${className}-bg` : ''}`;

			const columnValues = Object.keys(clusters).map((key) => {
				const embryo = clusters[key].embryo;
				const embryoLeftPosition = this.calculateEmbryoPositionX(columnIndex, clusters[key].implantationChance);

				return (
					<div key={key} className={`embryo${className ? ` ${className}` : ''}`} style={{ left: embryoLeftPosition }}>
						<div className="text">{key}<div>{clusters[key].implantationChance}%</div></div>

						<div className="pseudo-tooltip">
							{embryo && <img src={embryo.last_image_url} alt={`Cluster ${key} embryo`} />}
							<div className={tooltipClassName}>
								<p>Implantation chance: {clusters[key].implantationChance}%</p>
							</div>
						</div>
					</div>
				);
			});

			return (
				<td key={column}>{columnValues}</td>
			);
		});
	}

	renderDevelopmentStages() {
		const clustersByStage = this.props.clustersByStage;

		// Render Development Stages (Rows)
		return Object.keys(clustersByStage).map((key) => {
			const stageKey = key;
			const developmentStageName = clustersByStage[stageKey].name;

			// Render Row & Each Column

			const embryoColumns = this.renderEmbryos(stageKey);

			const divClassName= "tr-grey-tit";

			return (
				<tr key={key} className="embryos-table-row">
					<td>
						<div className={divClassName}>
							<span>{stageKey}</span>
							<span>{developmentStageName}</span>
						</div>
					</td>
					{embryoColumns}
				</tr>
			);
		});
	}


	render() {

		const developmentStages = this.renderDevelopmentStages();

		return (

				<div className="percentage-table">

					<div className="circle-items-line-wrapper">
						<span className="circle-item circle-item__green">Normal</span>
						<span className="circle-item circle-item__orange">Delayed</span>
						<span className="circle-item circle-item__yellow">Collapses</span>
						<span className="circle-item circle-item__brown">Low number</span>
						<span className="circle-item circle-item__red">Degenerated</span>
					</div>

						<div className="perc-two-side-arrow-wrapper">
							<div className="perc-for-two-sided-arrow">
								<span>70%</span>
								<span>60%</span>
								<span>50%</span>
								<span>40%</span>
								<span>30%</span>
								<span>20%</span>
								<span>10%</span>
								<span>0%</span>
							</div>
							<div className="two-sided-arrow"></div>
						</div>

						<div className="stage-of-dev">
							Stage of development
						</div>


						<table className="embryos-table impl-chance-table">

							{/* Embryos */}

							<tbody>
								{developmentStages}
							</tbody>

						</table>

					</div>

		);

	}

}


const mapStateToProps = (state) => ({
	clusterCategories: state.embryosLearningReducer.data.clusterCategories,
	clustersByStage: state.embryosLearningReducer.data.clustersByStage,
});

EmbryosTableByImplantanceChance.propTypes = {
	clusterCategories: PropTypes.object,
	clustersByStage: PropTypes.object,
};

export default connect(mapStateToProps)(EmbryosTableByImplantanceChance);

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import $ from 'jquery';

import { actions } from '../../../../../../../../reducers/embryos_library_reducer';

import EmbryoDetail from '../../../../../../../../components/EmbryoDetail';
import AddToCompareItem from '../AddToCompareItem';

import './styles.scss';


class EmbryoDetails extends Component {

	componentDidMount() {
		if (window.history.pushState) {
			window.history.pushState(null, document.title, `#embryo${this.props.embryo.id}`);
		}
	}

	componentWillUnmount() {
		if (this.element && $(window).scrollTop() > $(this.element).offset().top) {
			$(window).scrollTop($(window).scrollTop() - $(this.element).outerHeight(true));
		}
		if (window.history.pushState) {
			window.history.pushState(null, document.title, `${window.location.pathname}${window.location.search}`);
		}
	}

	handleElementRef = (ref) => {
		this.element = ref;
	};

	moveReferenceLine = (value) => {
		this.props.dispatch(actions.uiMoveCurrentSelectedEmbryoReferenceLine(value));
	};

	onUpArrowClick = (e) => {
		e.stopPropagation();
		this.props.dispatch(actions.uiUnselectEmbryo());
	};

	render() {
		const { embryo, currentCompareFirstEmbryo, currentCompareSecondEmbryo } = this.props;

		const isComparing = embryo.id === currentCompareFirstEmbryo.id || embryo.id === currentCompareSecondEmbryo.id;

		const embryosClusterClasses = [
			'embryos-row-cluster-container embryos-detail',
			isComparing && 'comparing',
		].filter(Boolean).join(' ');

		return (
			<div ref={this.handleElementRef} className={embryosClusterClasses}>
				<div className="embryos-detail__head">
					<h3>{this.props.embryo.cluster}</h3>
					<h4>{this.props.cluster.name}</h4>

					<AddToCompareItem
						embryo={this.props.embryo}
						currentCompareFirstEmbryo={this.props.currentCompareFirstEmbryo}
						currentCompareSecondEmbryo={this.props.currentCompareSecondEmbryo}
					/>

					<div className="cluster-id">
						{this.props.embryo.id}
					</div>

					<a className="close" onClick={this.onUpArrowClick}>&nbsp;</a>
				</div>

				<div className="embryos-detail__body">
					<p>{this.props.cluster.description}</p>

					<EmbryoDetail
						embryo={this.props.embryo}
						current={this.props.currentSelectedEmbryoReferenceLine}
						onReferenceLineChange={this.moveReferenceLine}
					/>
				</div>

				<div className="clearfix" />
			</div>
		);
	}

}

EmbryoDetails.propTypes = {
	embryo: PropTypes.object.isRequired,
	cluster: PropTypes.object.isRequired,

	currentCompareFirstEmbryo: PropTypes.object.isRequired,
	currentCompareSecondEmbryo: PropTypes.object.isRequired,

	currentSelectedEmbryoReferenceLine: PropTypes.number.isRequired,

	dispatch: PropTypes.func.isRequired,
};


const mapStateToProps = (state) => ({
	currentSelectedEmbryoReferenceLine: state.embryosLibraryReducer.currentSelectedEmbryoReferenceLine,
});

export default connect(mapStateToProps)(EmbryoDetails);

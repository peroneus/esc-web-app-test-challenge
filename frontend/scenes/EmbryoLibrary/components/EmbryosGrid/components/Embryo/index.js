import React, { PureComponent, Fragment } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import { actions } from '../../../../../../reducers/embryos_library_reducer';
import { getEmbryoFrameOffset } from '../../../../../../utils';

import SimpleChart from '../../../../../../components/SimpleChart';

import AddToCompareItem from './components/AddToCompareItem';
import EmbryoDetails from './components/EmbryoDetails';

import './styles.scss';

const q = {
	_interval: 1,
	_q: [],
	_nextId: 0,
	_tasks: {},
	_timer: null,
	_run: function () {
		if (this._timer !== null) {
			return;
		}
		this._timer = setTimeout(() => {
			this._timer = null;
			this._task();
		}, this._interval);
	},
	_task: function () {
		while (this._q.length) {
			const next = this._q.shift();
			if (this._tasks[next]) {
				this._tasks[next]();
				this._run();
				break;
			}
		}
	},
	add(task) {
		const id = this._nextId++;
		this._tasks[id] = task;
		this._q.push(id);
		this._run();
		return id;
	},
	remove(id) {
		if (this._tasks[id]) {
			delete this._tasks[id];
		}
	},
};


class Embryo extends PureComponent {

	constructor(props) {
		super(props);
		this.state = { renderChart: false };
		this.onEmbryoContainerClick = this.onEmbryoContainerClick.bind(this);
	}

	componentDidMount() {
		this.qId = q.add(() => this.setState({ renderChart: true }));
	}

	componentWillUnmount() {
		q.remove(this.qId);
	}

	onEmbryoContainerClick() {
		if (this.props.selected) {
			// Close the Embryo Details, because it's already open
			this.props.dispatch(actions.uiUnselectEmbryo());
		} else {
			// Open the Embryo Details
			this.props.dispatch(actions.uiSelectEmbryo(this.props.embryo));
		}
	}

	render() {
		const { embryo, currentCompareFirstEmbryo, currentCompareSecondEmbryo } = this.props;

		const isComparing = embryo.id === currentCompareFirstEmbryo.id || embryo.id === currentCompareSecondEmbryo.id;
		const embryoClasses = [
			'Block Embryo-block',
			isComparing && 'comparing',
			this.props.selected && 'select-embryo',
			this.props.listView && 'embryo-list-view',
		].filter(Boolean).join(' ');

		return (
			<Fragment>
				<div className={embryoClasses} onClick={this.onEmbryoContainerClick}>
					<div id={`embryo${embryo.id}`} className="pseudo-anchor" />

					<div className="Embryo-graph">
						{!this.props.listView ? (
							<img className="Embryo-image" alt={embryo.id} src={embryo.last_image_url} />
						) : null}

						{this.state.renderChart ? (
							<SimpleChart
								measurementCount={+embryo.image_count}
								offset={getEmbryoFrameOffset(embryo)}
								activityData={embryo.activity_data}
								expansionData={embryo.expansion_data}
							/>
						) : null}
					</div>

					<AddToCompareItem
						embryo={embryo}
						currentCompareFirstEmbryo={currentCompareFirstEmbryo}
						currentCompareSecondEmbryo={currentCompareSecondEmbryo}
					/>
				</div>

				{this.props.selected ? (
					<EmbryoDetails
						embryo={embryo}
						cluster={this.props.cluster}
						currentCompareFirstEmbryo={currentCompareFirstEmbryo}
						currentCompareSecondEmbryo={currentCompareSecondEmbryo}
					/>
				) : null}
			</Fragment>
		);
	}

}

Embryo.propTypes = {
	embryo: PropTypes.object.isRequired,
	cluster: PropTypes.object.isRequired,

	listView: PropTypes.bool.isRequired,

	currentCompareFirstEmbryo: PropTypes.object.isRequired,
	currentCompareSecondEmbryo: PropTypes.object.isRequired,

	selected: PropTypes.bool.isRequired,

	dispatch: PropTypes.func.isRequired,
};


export default connect()(Embryo);

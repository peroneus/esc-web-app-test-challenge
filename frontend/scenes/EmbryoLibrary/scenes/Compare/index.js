import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';

import { actions } from '../../../../reducers/embryos_library_reducer';
import { fetchEmbryoDetail } from '../../../../services/api';
import { getEmbryoFrameOffset } from '../../../../utils';

import EmbryosChart from '../../../../components/EmbryosChart';
import EmbryoImage from '../../../../components/EmbryoImage';
import Legend from '../../../../components/Legend';
import Timeline from '../../../../components/Timeline';

import buttonPause from '../../../../img/app-icons/icon-pausebutton.svg';
import buttonPlay from '../../../../img/app-icons/icon-playbutton.svg';
import iconCompareTwo from '../../../../img/icon-comparetwo.svg';

import './styles.scss';

const isEmpty = (obj) => Object.keys(obj).length === 0;


class Compare extends Component {

	_fetchingFirstEmbryo = null;

	_fetchingSecondEmbryo = null;

	componentDidMount() {
		const {
			currentCompareFirstEmbryo: firstEmbryo,
			currentCompareFirstCategory: firstCluster,
			currentCompareSecondEmbryo: secondEmbryo,
			currentCompareSecondCategory: secondCluster,
		} = this.props;

		const { idFirstEmbryo: firstId, idSecondEmbryo: secondId } = this.props.match.params;

		if (isEmpty(firstEmbryo) || isEmpty(firstCluster) || firstEmbryo.id !== firstId) {
			this._fetchingFirstEmbryo = firstId;
			this.props.dispatch(actions.uiRemoveFromCompareFirstEmbryo());
			fetchEmbryoDetail(firstId).then(({ embryo, cluster }) => {
				if (this._fetchingFirstEmbryo === embryo.id) {
					this.props.dispatch(actions.uiAddToCompareFirstEmbryo(embryo, cluster));
				}
			});
		}

		if (isEmpty(secondEmbryo) || isEmpty(secondCluster) || secondEmbryo.id !== secondId) {
			this._fetchingSecondEmbryo = secondId;
			this.props.dispatch(actions.uiRemoveFromCompareSecondEmbryo());
			fetchEmbryoDetail(secondId).then(({ embryo, cluster }) => {
				if (this._fetchingSecondEmbryo === embryo.id) {
					this.props.dispatch(actions.uiAddToCompareSecondEmbryo(embryo, cluster));
				}
			});
		}
	}

	componentDidUpdate() {
		const {
			currentCompareFirstEmbryo: firstEmbryo,
			currentCompareFirstCategory: firstCluster,
			currentCompareSecondEmbryo: secondEmbryo,
			currentCompareSecondCategory: secondCluster,
		} = this.props;

		if (this._fetchingFirstEmbryo && !isEmpty(firstEmbryo) && !isEmpty(firstCluster)) {
			this._fetchingFirstEmbryo = null;
		}

		if (this._fetchingSecondEmbryo && !isEmpty(secondEmbryo) && !isEmpty(secondCluster)) {
			this._fetchingSecondEmbryo = null;
		}
	}

	componentWillUnmount() {
		this._fetchingFirstEmbryo = null;
		this._fetchingSecondEmbryo = null;

		this.clearTimer();
		this.props.dispatch(actions.uiStopCompareChart(0));
	}

	getFrameStats() {
		const { currentCompareFirstEmbryo: firstEmbryo, currentCompareSecondEmbryo: secondEmbryo } = this.props;
		const firstOffset = getEmbryoFrameOffset(firstEmbryo);
		const secondOffset = getEmbryoFrameOffset(secondEmbryo);
		const firstImage = Math.min(firstOffset, secondOffset);
		const total = Math.max(firstOffset + (+firstEmbryo.image_count), secondOffset + (+secondEmbryo.image_count));
		return {
			firstEmbryoFrameOffset: firstOffset,
			secondEmbryoFrameOffset: secondOffset,
			firstImageFrame: firstImage,
			totalFrameCount: total,
		};
	}

	play() {
		const { currentComparePlay, currentComparePlayPosition } = this.props;
		if (currentComparePlay) {
			return;
		}

		const { firstImageFrame, totalFrameCount } = this.getFrameStats();
		const frame = Math.min(Math.max(currentComparePlayPosition, firstImageFrame), totalFrameCount - 1);

		this.clearTimer();
		this.props.dispatch(actions.uiPlayCompareChart(frame));
		this.timer = setInterval(this.tick, 200);
	}

	stop() {
		const { currentComparePlay, currentComparePlayPosition } = this.props;

		if (!currentComparePlay) {
			return;
		}

		const { totalFrameCount } = this.getFrameStats();
		const position = currentComparePlayPosition < totalFrameCount - 1 ? currentComparePlayPosition : 0;

		this.clearTimer();
		this.props.dispatch(actions.uiStopCompareChart(position));
	}

	close() {
		this.props.history.push('/embryo_library');
	}

	clearTimer() {
		if (this.timer) {
			clearInterval(this.timer);
			delete this.timer;
		}
	}

	tick = () => {
		const { currentComparePlay, currentComparePlayPosition } = this.props;
		if (!currentComparePlay) {
			return;
		}

		const { firstImageFrame, totalFrameCount } = this.getFrameStats();
		const frame = Math.max(currentComparePlayPosition + 1, firstImageFrame);

		if (frame < totalFrameCount) {
			this.props.dispatch(actions.uiPlayCompareChart(frame));
		} else {
			this.stop();
		}
	};

	onPlayButtonClick = () => {
		if (this.props.currentComparePlay) {
			this.stop();
		} else {
			this.play();
		}
	};

	onRemoveFirstEmbryoClick = () => {
		this.props.dispatch(actions.uiRemoveFromCompareFirstEmbryo());
		this.close();
	};

	onRemoveSecondEmbryoClick = () => {
		this.props.dispatch(actions.uiRemoveFromCompareSecondEmbryo());
		this.close();
	};

	onCloseButtonClick = () => {
		this.close();
	};

	moveReferenceLine = (value) => {
		if (!this.props.currentComparePlay) {
			this.props.dispatch(actions.uiMoveCurrentCompareEmbryoReferenceLine(value));
		}
	};

	render() {
		const {
			currentCompareFirstEmbryo: firstEmbryo,
			currentCompareFirstCategory: firstCluster,

			currentCompareSecondEmbryo: secondEmbryo,
			currentCompareSecondCategory: secondCluster,

			currentComparePlay,
			currentComparePlayPosition,
		} = this.props;

		if (isEmpty(firstEmbryo) || isEmpty(firstCluster) || isEmpty(secondEmbryo) || isEmpty(secondCluster)) {
			return null;
		}

		const { firstEmbryoFrameOffset, secondEmbryoFrameOffset, totalFrameCount } = this.getFrameStats();

		return (
			<div id="compare-detail" className="container-fluid">
				<div className="row">
					<div className="col-12 text-center">
						<span id="compare-header">
							<img alt="" src={iconCompareTwo} />
							Embryo compare
						</span>
						<div className="close" onClick={this.onCloseButtonClick}>Close</div>
					</div>
				</div>

				<hr />

				{/* First Embryo */}
				<div className="row">
					<div className="col-12 col-xl-3 col-lg-4">
						<EmbryoImage
							embryo={firstEmbryo}
							offset={firstEmbryoFrameOffset}
							currentSelectedEmbryoReferenceLine={currentComparePlayPosition}
						/>
					</div>

					<div className="col-12 col-xl-9 col-lg-8">
						<div className="inside">
							{/* Top Box */}
							<div className="top-box">
								<div className="title-box">
									<h3>{firstCluster.name}</h3>
									<div className={`info tooltip-top tooltip-top__large tooltip-netvor cluster-bg-color-${firstEmbryo.cluster}`}>
										<span>{firstEmbryo.cluster}</span>
									</div>
									<div className="cluster-id">{firstEmbryo.id}</div>
								</div>

								<div className="right-box">
									<a className="remove-compare-embryo" onClick={this.onRemoveFirstEmbryoClick}>Remove from compare</a>
								</div>
							</div>

							<p>{firstCluster.description}</p>

							<EmbryosChart
								measurementCount={+firstEmbryo.image_count}
								offset={firstEmbryoFrameOffset}
								totalCount={totalFrameCount}
								expansionData={firstEmbryo.expansion_data}
								activityData={firstEmbryo.activity_data}
								events={firstEmbryo.events}
								embryoReferenceLine={currentComparePlayPosition}
								onReferenceLineChange={this.moveReferenceLine}
							/>

							<Legend />
						</div>
					</div>
				</div>

				{/* Play/Pause Button */}
				<div className="row">
					<div className="col-12 col-xl-3 col-lg-4">
						<div className="play-btn" onClick={this.onPlayButtonClick}>
							{currentComparePlay ? <img src={buttonPause} alt='Pause' /> : <img src={buttonPlay} alt='Play' />}
						</div>
					</div>

					<div className="col-12 col-xl-9 col-lg-8">
						<div className="compare-embryo-track">
							<Timeline
								count={totalFrameCount}
								current={currentComparePlayPosition || null}
							/>
						</div>
					</div>
				</div>

				{/* Second Embryo */}
				<div className="row">
					<div className="col-12 col-xl-3 col-lg-4">
						<EmbryoImage
							embryo={secondEmbryo}
							offset={secondEmbryoFrameOffset}
							currentSelectedEmbryoReferenceLine={currentComparePlayPosition}
						/>
					</div>

					<div className="col-12 col-xl-9 col-lg-8">
						<div className="inside">
							<EmbryosChart
								measurementCount={+secondEmbryo.image_count}
								offset={secondEmbryoFrameOffset}
								totalCount={totalFrameCount}
								expansionData={secondEmbryo.expansion_data}
								activityData={secondEmbryo.activity_data}
								events={secondEmbryo.events}
								embryoReferenceLine={currentComparePlayPosition}
								onReferenceLineChange={this.moveReferenceLine}
							/>

							<Legend />

							{/* Top Box */}
							<div className="top-box">
								<div className="title-box">
									<h3>{secondCluster.name}</h3>
									<div className={`info tooltip-top tooltip-top__large tooltip-netvor cluster-bg-color-${secondEmbryo.cluster}`}>
										<span>{secondEmbryo.cluster}</span>
									</div>
									<div className="cluster-id">{secondEmbryo.id}</div>
								</div>

								<div className="right-box">
									<a className="remove-compare-embryo" onClick={this.onRemoveSecondEmbryoClick}>Remove from compare</a>
								</div>
							</div>

							<p>{secondCluster.description}</p>
						</div>
					</div>
				</div>
			</div>
		);
	}

}

Compare.propTypes = {
	currentCompareFirstEmbryo: PropTypes.object.isRequired,
	currentCompareFirstCategory: PropTypes.object.isRequired,

	currentCompareSecondEmbryo: PropTypes.object.isRequired,
	currentCompareSecondCategory: PropTypes.object.isRequired,

	currentComparePlay: PropTypes.bool,
	currentComparePlayPosition: PropTypes.number,

	dispatch: PropTypes.func.isRequired,
};

Compare.defaultProps = {
	currentComparePlay: false,
	currentComparePlayPosition: 0,
};


const mapStateToProps = (state) => ({
	currentCompareFirstEmbryo: state.embryosLibraryReducer.currentCompareFirstEmbryo,
	currentCompareFirstCategory: state.embryosLibraryReducer.currentCompareFirstCategory,

	currentCompareSecondEmbryo: state.embryosLibraryReducer.currentCompareSecondEmbryo,
	currentCompareSecondCategory: state.embryosLibraryReducer.currentCompareSecondCategory,

	currentComparePlay: state.embryosLibraryReducer.currentComparePlay,
	currentComparePlayPosition: state.embryosLibraryReducer.currentComparePlayPosition,
});

export default withRouter(connect(mapStateToProps)(Compare));

import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

import Timeline from './Timeline';


export default class MytoticCycle extends PureComponent {

	static propTypes = {
		onVisitPage: PropTypes.func.isRequired,
	};

	componentDidMount() {
		this.props.onVisitPage();
	}

	render() {
		return (
			<div className="col content" id="first-part-show">
				<div className="head">
					<h1>Mytotic cycle</h1>
					<p>Due to inability to monitor interphase properly, we consider mytotic cycle as a period between first division in givengeneration till the first division in the next generation. New generation starts when the previous generation multiplies by two. Understanding mytotic cycle and how we perceive it is key for embryo evaluation. Standard definition is however not what we monitor. This is due to technical issues.</p>
				</div>

				<div className="full">
					<hr />
					<div className="wrap">
						<div className="zero column">
							<div>
								<div className="bub bub-w">
									M0
								</div>
							</div>
						</div>
						<div className="column">
							<div>
								<div className="bub bub-g">
									M1.1
								</div>
								<div className="bub bub-g">
									M1.2
								</div>
							</div>
						</div>
						<div className="column double">
							<div>
								<div className="bub bub-b">
									M2.1
								</div>
								<div className="bub bub-b">
									M2.2
								</div>
							</div>
							<div>
								<div className="bub bub-b">
									M2.3
								</div>
								<div className="bub bub-b">
									M2.4
								</div>
							</div>
							<div className="highlighting mytcy" data-highlight="Mytotic cycle" />
						</div>
					</div>

					<Timeline stepsCount={80} />
				</div>
			</div>
		);
	}

}

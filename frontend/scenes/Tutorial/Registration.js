import React, { PureComponent } from 'react';

import trainingScreen from './images/training.jpg';
import '../../login';


export default class Registration extends PureComponent {

	render() {
		return (
			<div className="col content" id="registration">
				<div className="head">
					<h1>Register</h1>
					<p>Do you like what you have just seen? Just create an account and continue in browsing thousands of embryos or study how the system works. We are constantly adding new features so keep an eye on ESCAPE.</p>
				</div>
				<hr />

				<div className="box">
					<div className="sh-box half">
						<form className="user-form" >
							<div className="form-group">
								<input placeholder="First name" type="text" name="firstName" id="frm-registrationForm-firstName" />
								<label htmlFor="frm-registrationForm-firstName">First name</label>
							</div>

							<div className="form-group">
								<input placeholder="Last name" type="text" name="lastName" id="frm-registrationForm-lastName" required="" />
								<label htmlFor="frm-registrationForm-lastName">Last name</label>
							</div>
							<div className="clearfix" />

							<div className="form-group">
								<div className="select">
									<select name="clinic" id="frm-registrationForm-clinic"><option value="">I am an independent embryologist</option></select>
									<label htmlFor="frm-registrationForm-clinic">Clinic where you work</label>
								</div>
							</div>

							<div className="form-group margin-t">
								<input placeholder="E-mail" type="text" name="email" id="frm-registrationForm-email" required="" />
								<label htmlFor="frm-registrationForm-email">E-mail</label>
							</div>

							<div className="form-group">
								<input placeholder="Password" type="password" name="password" id="frm-registrationForm-password" required="" />
								<label htmlFor="frm-registrationForm-password">Password</label>
							</div>

							<div className="form-group">
								<input placeholder="Password confirm" type="password" name="passwordConfirm" id="frm-registrationForm-passwordConfirm" required="" />
								<label htmlFor="frm-registrationForm-passwordConfirm">Password (again)</label>
							</div>

							<div className="clearfix" />

							<div className="form-group left">
								<div className="checkbox">
									<input type="checkbox" name="agree" id="frm-registrationForm-agree" required="" className="has-value" />
									<span />
								</div>
								<label htmlFor="frm-registrationForm-agree">I agree with <a href="">terms and conditions</a></label>
							</div>

							<div className="clearfix" />
							<button className="btn btn-orange" type="submit" name="_submit" value="Submit">REGISTER</button>
						</form>
					</div>

					<div className="ab-bg">
						<div className="screen-border">
							<img src={trainingScreen} alt="training" />
						</div>
						<div className="screen-border">
							<img src={trainingScreen} alt="training" />
						</div>
					</div>
				</div>
			</div>
		);
	}

}

import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

import GraphIconThree from '../../components/GraphIconThree';
import Timeline from './Timeline';


export default class TimingOfCleavage extends PureComponent {

	static propTypes = {
		onVisitPage: PropTypes.func.isRequired,
	};

	componentDidMount() {
		this.props.onVisitPage();
	}

	render() {
		return (
			<div className="col content" id="first-part-show">
				<div className="head">
					<div className="btn-white right">
						<GraphIconThree />
						<span>Cleavage curve</span>
					</div>

					<h1>Timing of cleavage</h1>
					<p>The stair-like line in the bottom annotates cleavages (cell divisions) occurring in the embryo. As the cells divide, the curve grows. It helps us to monitor cell count which is important part of the evaluation.</p>
				</div>

				<div className="full">
					<hr />
					<div className="wrap">
						<div className="zero column">
							<div>
								<div className="bub bub-w">
									M0
								</div>
							</div>
							<span>1</span>
						</div>

						<div className="column">
							<div>
								<div className="bub bub-g">
									M1.1
								</div>
								<div className="bub bub-g">
									M1.2
								</div>
							</div>
							<span>2</span>
						</div>

						<div className="column double">
							<div>
								<div className="bub bub-b">
									M2.1
								</div>
								<div className="bub bub-b">
									M2.2
								</div>
							</div>
							<div>
								<div className="bub bub-b">
									M2.3
								</div>
								<div className="bub bub-b">
									M2.4
								</div>
							</div>
							<span>3</span>
							<span>4</span>
						</div>

						<div className="column double half">
							<div>
								<div className="bub bub-y">
									M3.1
								</div>
								<div className="bub bub-y">
									M3.2
								</div>
							</div>
							<div>
								<div className="bub bub-y">
									M3.3
								</div>
								<div className="bub bub-y">
									M3.4
								</div>
							</div>
							<span>5</span>
							<span>6</span>
						</div>
					</div>

					<Timeline stepsCount={100} />
				</div>
			</div>
		);
	}
}

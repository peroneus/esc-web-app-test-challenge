import React, { PureComponent } from 'react';

import ArrowDownGraph from '../../../components/ArrowDownGraph';
import ArrowUpGraph from '../../../components/ArrowUpGraph';
import GraphIconOne from '../../../components/GraphIconOne';
import GraphIconTwo from '../../../components/GraphIconTwo';
import GraphIconThree from '../../../components/GraphIconThree';
import InfoIcon from '../../../components/InfoIcon';

import './index.scss';


export default class Legend extends PureComponent {

	render() {
		return (
			<div className="info-wrap">
				<div className="btn-legend">
					<InfoIcon />
					Legend
				</div>

				<div className="legend-body">
					<div className="column">
						<div className="item-type-wrapper">
							<ArrowUpGraph color="#A75FD2" />
							<div className="info-text-wrapper">
								<p className="heading">
									Start of expansion
								</p>
								<p className="text">
									Purple arrow marks <span className="bolder-text">where embryo expansion starts</span>.
								</p>
							</div>
						</div>

						<div className="item-type-wrapper">
							<ArrowDownGraph color="#61B945" />
							<div className="info-text-wrapper">
								<p className="heading">
									Green collapse
								</p>
								<p className="text">
									<span className="bolder-text">Very mild</span> collapses or mitotic pulses – correspond with the group cell cleavages
								</p>
							</div>
						</div>

						<div className="item-type-wrapper">
							<ArrowDownGraph color="#39A4CD"/>
							<div className="info-text-wrapper">
								<p className="heading">
									Blue collapse
								</p>
								<p className="text">
									<span className="bolder-text">Mild</span> collapses or mitotic pulses – correspond with the group cell cleavages
								</p>
							</div>
						</div>

						<div className="item-type-wrapper">
							<ArrowDownGraph color="#D7D721"/>
							<div className="info-text-wrapper">
								<p className="heading">
									Yellow collapse
								</p>
								<p className="text">
									<span className="bolder-text">More extensive</span> collapses or mitotic pulses of otherwise correctly cleaving embryos – correspond with the group cell cleavages
								</p>
							</div>
						</div>
					</div>

					<div className="column">
						<div className="item-type-wrapper">
							<ArrowDownGraph color="#E13E52"/>
							<div className="info-text-wrapper">
								<p className="heading">
									Red collapse
								</p>
								<p className="text">
									<span className="bolder-text">Extensive</span> collapses induced by apoptotic cells elimination – the consequence of the previous abnormal cleavages
								</p>
							</div>
						</div>

						<div className="item-type-wrapper">
							<GraphIconOne />
							<div className="info-text-wrapper">
								<p className="heading">
									Segmentation curve
								</p>
								<p className="text">
									Expression of <span className="bolder-text">embryo growth</span> – area of embryo in pixels
								</p>
							</div>
						</div>

						<div className="item-type-wrapper">
							<GraphIconTwo />
							<div className="info-text-wrapper">
								<p className="heading">
									Activity curve
								</p>
								<p className="text">
									Expression of the <span className="bolder-text">cell changes</span> in time – correlates with the cell cleavages
								</p>
							</div>
						</div>

						<div className="item-type-wrapper">
							<GraphIconThree />
							<div className="info-text-wrapper">
								<p className="heading">
									Cleavages
								</p>
								<p className="text">
									The <span className="bolder-text">number of the cells</span> at specified time
								</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		);
	}

}

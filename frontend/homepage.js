import $ from 'jquery';
import 'bootstrap';
import 'font-awesome/scss/font-awesome.scss';

import 'slick-carousel';
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';

import registerServiceWorker from './registerServiceWorker';

import './homepage.scss';

registerServiceWorker();

let windowHeight;

$(function initBootstrap() {
	initFirstPriority();
	initBootstrapSmoothScrolling();
	initArrowUpAndFixedMenu();
	initCarousel();
	initCustomSwitchableTabs();
});

function initFirstPriority() {
	windowHeight = $(window).height();
	$('.nav.nav-fixed').fadeOut().removeClass('d-none');
	// when user resize the window - recalculate the window height so anchors / menu etc. will be valid
	window.addEventListener('resize', () => {
		windowHeight = $(window).height();
	});
}

function initBootstrapSmoothScrolling() {
	$("ul.navigation-ours li a[href^='#']").on('click', function (e) {
		e.preventDefault();
		const hash = this.hash;
		const offsetTop = $(hash).offset().top;

		$('html, body').animate({
			scrollTop: offsetTop
		}, 300, function () {
			window.location.hash = hash;
		});
	});
}

function initArrowUpAndFixedMenu() {
	// hide .back-to-top first
	$('.back-to-top-1').hide();

	$('.back-to-top').bind('click', function () {
			$(this).addClass('pseudo-hover').delay(1000).queue(function () {
			$(this).removeClass('pseudo-hover').dequeue();
		});
	});

	// fade in .back-to-top
	$(window).scroll(function () {
		if ($(this).scrollTop() >= windowHeight - 98) {
			$('.back-to-top').addClass("visible");
			$('.back-to-top').fadeIn();

			$('.nav.nav-fixed').fadeIn();
		} else {
			$('.back-to-top-1').fadeOut();
			$('.back-to-top-1').removeClass("visible");

			$('.nav.nav-fixed').fadeOut();
		}
	});

	// scroll body to 0px on click
	$('a .back-to-top').click(function () {
		$('body,html').animate({
			scrollTop: 0
		}, 800);
		return false;
	});
}

/* settings for slick carousel */
function initCarousel() {

	$('.carousel').slick({
		centerMode: true,
		centerPadding: '10px',
		slidesToShow: 3,
		arrows: false,
		dots: true,
		variableWidth: true,
		autoplay: true,
		autoplaySpeed: 4000,
		responsive: [
		{
			breakpoint: 1365,
				settings: {
				centerMode: true,
				centerPadding: '41px',
				slidesToShow: 3
			}
		},
		{
			breakpoint: 1000,
			settings: {
				centerMode: true,
				centerPadding: '41px',
				slidesToShow: 1
			}
		}
		]
	});

	let actualPosition = 0;

	$('#carousel-indicators li').on('click', function () {
		const el = $(this);
		const position = el.data('position');

	$('.carousel').on('beforeChange', function () {
		// prevent flashing when its in same position
		if(actualPosition !== position) {
			$('#carousel-indicators li').removeClass('active');
			el.addClass('active');
			actualPosition = position;
		}
	});

	$('.carousel').slick('slickGoTo', position, 'true');

	});
}

function initCustomSwitchableTabs() {
	$("a[href^='#'].embryo").on('click', function(e) {
		e.preventDefault();
		const prevId = $("a[href^='#'].embryo.active").attr('id');
		const elem = $(this);
		const currentId = elem.attr("id");
		const altText = elem.data('alt');

		$("a[href^='#'].embryo").removeClass('active');
		elem.addClass('active');

		const contentEl = $('.tab-content').find("[data-tab='" + currentId + "']");
		$('.embryo-content').removeClass('show active');
		$('.embryo-content').hide();
		contentEl.addClass('show active');
		contentEl.show();

		const sliderEls = $('.slick-slide img');
		sliderEls.each( function() {
			const imgUrl = $(this).attr('src').replace(prevId, currentId);
			$(this).replaceWith('<img src="' + imgUrl + '" alt="' + altText +'" >');
		});
	});
}


import { combineReducers } from 'redux';

import embryosLearningReducer from './embryos_learning_reducer';
import embryosLibraryReducer from './embryos_library_reducer';
import embryosTrainingReducer from './embryos_training_reducer';
import embryosTutorialReducer from './embryos_tutorial_reducer';


const rootReducer = combineReducers({
	embryosLearningReducer,
	embryosLibraryReducer,
	embryosTrainingReducer,
	embryosTutorialReducer
});

export default rootReducer;


export const types = {

	EMBRYOS_LEARNING_REQUESTED: 'EMBRYOS_LEARNING_REQUESTED',
	EMBRYOS_LEARNING_SUCCEEDED: 'EMBRYOS_LEARNING_SUCCEEDED',
	EMBRYOS_LEARNING_FAILED: 'EMBRYOS_LEARNING_FAILED',

	EMBRYOS_LEARNING_VISITED: 'EMBRYOS_LEARNING_VISITED',
	EMBRYOS_LEARNING_LEFT: 'EMBRYOS_LEARNING_LEFT',

	EMBRYOS_LEARNING_SELECT_STAGE: 'EMBRYOS_LEARNING_SELECT_STAGE',

	EMBRYOS_LEARNING_SELECT_QUALITY: 'EMBRYOS_LEARNING_SELECT_QUALITY',

	EMBRYOS_LEARNING_SELECT_EVENT: 'EMBRYOS_LEARNING_SELECT_EVENT',

	EMBRYOS_LEARNING_STAGE_DEVELOPMENT_MOVE_REFERENCE_LINE: 'EMBRYOS_LEARNING_STAGE_DEVELOPMENT_MOVE_REFERENCE_LINE',
	EMBRYOS_LEARNING_QUALITY_MOVE_REFERENCE_LINE: 'EMBRYOS_LEARNING_QUALITY_MOVE_REFERENCE_LINE',
	EMBRYOS_LEARNING_EVENTS_MOVE_REFERENCE_LINE: 'EMBRYOS_LEARNING_EVENTS_MOVE_REFERENCE_LINE',

	EMBRYOS_LEARNING_CHANGE_IMPLANTANCE_CHANCE_VIEW: 'EMBRYOS_LEARNING_CHANGE_IMPLANTANCE_CHANCE_VIEW'

};


export const initialState = {

	data: {
		introductionVisited: false,
		stagesOfDevelopmentVisited: false,
		embryoQualityVisited: false,
		eventsVisited: false,
		implantationChanceVisited: false,
		clusterCategories: {},
		clustersByStage: {},
	},

	currentDevelopmentStage: {},
	currentDevelopmentStagePlayPosition: 0,

	currentEmbryoQuality: {},
	currentQualityPlayPosition: 0,

	currentEvent: {},
	currentEventPlayPosition: 0,

	currentImplantanceChanceView: "table",

	isLoading: false,
	error: null

};

export default function embryosLearningReducer(state = initialState, action) {

	switch(action.type) {

		case types.EMBRYOS_LEARNING_REQUESTED: {

			return Object.assign({}, state, {
				isLoading: true
			})

		}

		case types.EMBRYOS_LEARNING_SUCCEEDED: {

			const clustersByStage = action.payload.clustersByStage;
			const defaultStageKey = Object.keys(clustersByStage)[0];
			const defaultStage = clustersByStage[defaultStageKey];

			const defaultQualityKey = Object.keys(defaultStage.clustersByQuality)[0];
			const defaultQuality =  defaultStage.clustersByQuality[defaultQualityKey];

			const defaultEventKey = Object.keys(defaultQuality.clustersByEvents)[0];
			const defaultEvent =  defaultQuality.clustersByEvents[defaultEventKey];

			return Object.assign({}, state, {

				data: action.payload,
				currentDevelopmentStage: defaultStage,
				currentEmbryoQuality: defaultQuality,
				currentEvent: defaultEvent,
				isLoading: false

			})

		}

		case types.EMBRYOS_LEARNING_FAILED: {

			return Object.assign({}, state, {
				isLoading: false,
				error: action.error
			})

		}

		case types.EMBRYOS_LEARNING_LEFT: {
			switch (action.section) {
				case 'introduction':
				case 'stagesOfDevelopment':
				case 'embryoQuality':
				case 'events':
				case 'implantationChance':
					return {
						...state,
						data: {
							...state.data,
							[`${action.section}Visited`]: true
						},
					};
				default:
					return state;
			}
		}

		case types.EMBRYOS_LEARNING_SELECT_STAGE: {

			let currentEmbryoQuality = null;

			const clustersByQuality = action.payload.clustersByQuality;
			if(Object.keys(clustersByQuality).length > 0) {

				const key = Object.keys(clustersByQuality)[0];
				currentEmbryoQuality = clustersByQuality[key];

			}

			const clustersByEvents = currentEmbryoQuality && currentEmbryoQuality.clustersByEvents;
			const currentEventKey = clustersByEvents && Object.keys(clustersByEvents)[0];
			const currentEvent = (currentEventKey && clustersByEvents[currentEventKey]) || null;

			return Object.assign({}, state, {
				currentDevelopmentStage: action.payload,
				currentEmbryoQuality,
				currentEvent,
			});

		}

		case types.EMBRYOS_LEARNING_SELECT_QUALITY: {

			let currentEvent = null;
			const clustersByEvents = action.payload.clustersByEvents;

			if(Object.keys(clustersByEvents).length > 0) {

				const key = Object.keys(clustersByEvents)[0];
				currentEvent = clustersByEvents[key];

			}

			return Object.assign({}, state, {
				currentEmbryoQuality: action.payload,
				currentEvent
			})

		}

		case types.EMBRYOS_LEARNING_SELECT_EVENT: {

			return Object.assign({}, state, {
				currentEvent: action.payload
			})

		}

		// Stage Development Section : Reference Line

		case types.EMBRYOS_LEARNING_STAGE_DEVELOPMENT_MOVE_REFERENCE_LINE: {

			return Object.assign({}, state, {
					currentDevelopmentStagePlayPosition: action.payload
			})

		}

		// Embryos Quality Section : Reference Line

		case types.EMBRYOS_LEARNING_QUALITY_MOVE_REFERENCE_LINE: {

			return Object.assign({}, state, {
					currentQualityPlayPosition: action.payload
			})

		}


		// Embryos Events Section : Reference Line

		case types.EMBRYOS_LEARNING_EVENTS_MOVE_REFERENCE_LINE: {

			return Object.assign({}, state, {
					currentEventPlayPosition: action.payload
			})

		}

		// Embryos Implantance Chance Section: View

		case types.EMBRYOS_LEARNING_CHANGE_IMPLANTANCE_CHANCE_VIEW: {

			return Object.assign({}, state, {
					currentImplantanceChanceView: action.payload
			})

		}



		default:
			return state


	}

}

export const actions = {

	requestEmbryosLearning: () => ({ type: types.EMBRYOS_LEARNING_REQUESTED }),
	requestSucceededEmbryosLearning : (data) => ({ type: types.EMBRYOS_LEARNING_SUCCEEDED, payload: data }),
	requestFailedEmbryosLearning : (errorMessage) => ({ type: types.EMBRYOS_LEARNING_FAILED, error: errorMessage }),

	visitEmbryosLearning: (section) => ({ type: types.EMBRYOS_LEARNING_VISITED, section }),
	leaveEmbryosLearning: (section) => ({ type: types.EMBRYOS_LEARNING_LEFT, section }),

	uiSelectStage: (data) => ({ type: types.EMBRYOS_LEARNING_SELECT_STAGE, payload: data }),
	uiSelectQuality: (data) => ({ type: types.EMBRYOS_LEARNING_SELECT_QUALITY, payload: data }),
	uiSelectEvent: (data) => ({ type: types.EMBRYOS_LEARNING_SELECT_EVENT, payload: data }),

	uiMoveCurrentStageDevelopmentReferenceLine: (value) => ({ type: types.EMBRYOS_LEARNING_STAGE_DEVELOPMENT_MOVE_REFERENCE_LINE, payload: value }),
	uiMoveCurrentQualityReferenceLine: (value) => ({ type: types.EMBRYOS_LEARNING_QUALITY_MOVE_REFERENCE_LINE, payload: value }),
	uiMoveCurrentEventReferenceLine: (value) => ({ type: types.EMBRYOS_LEARNING_EVENTS_MOVE_REFERENCE_LINE, payload: value }),

	uiChangeViewImplantanceChance: (value) => ({ type: types.EMBRYOS_LEARNING_CHANGE_IMPLANTANCE_CHANCE_VIEW, payload: value })

};


/* @flow */
export type TCbBloodGroup = {
  id: string,
  label: string
}

/* @flow */
export type TCbEyeColor = {
  id: string,
  label: string
}

/* @flow */
export type TCbEmbryoEvent = {
  id: string,
  label: string,
  color: string
}

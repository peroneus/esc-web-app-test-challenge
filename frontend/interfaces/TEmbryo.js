/* @flow */
import type { TEmbryoEvent } from './TEmbryoEvent';
import type { TPetriDish } from './TPetriDish';
import type { TCbCluster } from './cb/TCbCluster';
import type { TCbAction } from './cb/TCbAction';

export type TEmbryo = {
  id: string,
  fertilized: boolean,
  catiComputedCluster: TCbCluster,
  cluster: TCbCluster,
  clusterTimeline: Array<TCbCluster>,
  catiProposedAction: TCbAction,
  proposedAction: TCbAction,
  ownEggs: boolean,
  ownSperm: boolean,
  petriDish: TPetriDish,
  petriDishPosition: number,
  activity: Array<number>,
  expansion: Array<number>,
  events: Array<TEmbryoEvent>,
  lastImagePath: string,
  imagesPath: string
};

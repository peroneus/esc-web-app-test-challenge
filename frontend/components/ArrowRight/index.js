import React, { Component } from 'react';
import './styles.scss';

class ArrowRight extends Component {

	render() {

			return (
			<div className="arrow-right-icon">
				<svg width="20px" height="18px" viewBox="0 0 20 18" className={this.props.direction}>
					<g id="Icons" stroke="none" strokeWidth="1" fill="none" fillRule="evenodd" transform="translate(-711.000000, -843.000000)" strokeLinecap="round" strokeLinejoin="round">
						<g id="arrow-right" transform="translate(713.000000, 845.000000)" stroke="#C3C3C3" strokeWidth="2.5">
							<path d="M0,7 L16,7" id="Shape"></path>
							<polyline id="Shape" points="9 0 16 7 9 14"></polyline>
						</g>
					</g>
				</svg>
			</div>

			);

	}

}

export default ArrowRight;

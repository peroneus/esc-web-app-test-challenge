import React, { Component } from 'react';

import InfoIcon from '../InfoIcon';

import '../Legend/styles.scss';
import './styles.scss';

class LegendSuccessRate extends Component {

	render() {

		return (

			<div className="info-wrapper legend-success-wrapper">
					<div className="legend-text-wrap">
						<InfoIcon />
					</div>
					<p className="pseudo-label-legend">
						Legend
					</p>
					<div className="pseudo-info-tooltip">
						<div className="item-type-wrapper">
							<div className="info-text-wrapper">
								<p className="text">
									<span className="bolder-text">Success rate is the ratio between correct and wrong answers in the last {this.props.lastInstances} instances.</span>
								</p>
							</div>
						</div>
					</div>
				</div>

		);

	}

}

export default LegendSuccessRate;

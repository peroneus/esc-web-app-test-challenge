import React, { Component } from 'react';
import Lottie from 'react-lottie';
import * as animationData from './loading.json'

class Loading extends Component {

	render() {

	const defaultOptions = {
		loop: true,
		autoplay: true,
		animationData: animationData,
	};

		return (

			<div className="container-fluid">
				<div className="loader-wrapper">
						<Lottie preserveAspectRatio="xMidYMid slice" options={defaultOptions}
						height={300}
						width={300}
						isStopped={false}
						isPaused={false} />
				</div>
			</div>

		);
	}

}

export default Loading;

import React, { Component } from 'react';
import './styles.scss';

class ArrowToTop extends Component {

	render() {

			return (
				<a id="arrow-to-up" href="#top" className={this.props.className}>
					<div className="back-to-top back-to-top-1">
						<svg width="45px" height="45px" viewBox="0 0 53 53">
							<g id="Icons" stroke="none" strokeWidth="1" fill="none" fillRule="evenodd" transform="translate(-647.000000, -975.000000)">
								<g id="autoscroll-up" transform="translate(651.000000, 977.000000)">
									<g id="Group-4">
										<g id="Rectangle-12">
										</g>
										<path d="M12.6726728,28.7399401 C12.2640152,29.111447 11.6315669,29.0813304 11.2600599,28.6726728 C10.888553,28.2640152 10.9186696,27.6315669 11.3273272,27.2600599 L22.3273272,17.2600599 C22.7087484,16.9133134 23.2912516,16.9133134 23.6726728,17.2600599 L34.6726728,27.2600599 C35.0813304,27.6315669 35.111447,28.2640152 34.7399401,28.6726728 C34.3684331,29.0813304 33.7359848,29.111447 33.3273272,28.7399401 L23,19.3514608 L12.6726728,28.7399401 Z" id="Shape" fill="#F3A16A" fillRule="nonzero"></path>
									</g>
								</g>
							</g>
						</svg>
					</div>
				</a>
			);
	}

}

export default ArrowToTop;

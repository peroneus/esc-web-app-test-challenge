import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

import './styles.scss';

const PREVIEW_COUNT = 7;

class EmbryosImagesMilestones extends PureComponent {

	getImageFrames() {
		const imageCount = +this.props.embryo.image_count;
		const totalCount = this.props.totalCount || (imageCount + this.props.offset);
		const current = this.props.currentSelectedEmbryoReferenceLine;
		const activeFrame = current && Math.ceil((current * PREVIEW_COUNT) / (totalCount - 1)) - 1;

		const result = [];

		for (let i = 0; i < PREVIEW_COUNT; i++) {
			const frame = Math.floor((totalCount - 1) * (i + 1) / PREVIEW_COUNT);
			const active = i === activeFrame;

			const backgroundFrame = Math.max(0, Math.min(imageCount - 1, (active ? current : frame) - this.props.offset));

			const element = <div key={i} className="bg-img" style={{
				opacity: active ? 1.0 : 0.4,
				backgroundImage: `url(${this.props.embryo.image_url})`,
				backgroundPositionX: `${backgroundFrame / (imageCount - 1) * 100}%`
			}} />;

			result.push(element);
		}

		return result;
	}

	render() {
		const images = this.getImageFrames();
		const imageCount = this.props.embryo.image_count;

		return (
			<div className="embryo-images-over-graph">
				<div className="embryos-milestones" style={{ margin: `0 ${100 / (2 * imageCount)}%`}}>
					{images}
				</div>
				<div className="clearfix" />
			</div>
		);
	}

}

EmbryosImagesMilestones.propTypes = {
	embryo: PropTypes.object,
	offset: PropTypes.number,
	totalCount: PropTypes.number,
	currentSelectedEmbryoReferenceLine: PropTypes.number,
};

EmbryosImagesMilestones.defaultProps = {
	offset: 0,
	totalCount: null,
	currentSelectedEmbryoReferenceLine: null,
};

export default EmbryosImagesMilestones;

import React, { Component } from 'react';

import './styles.scss';
import logo from '../../img/icon-esclogo.svg';

class ErrorSection extends Component {

	render() {

		return (

			<div className="error-section-container">
				<img src={logo} alt="" />
				<h3 id="error-heading">Something went wrong</h3>
				<a className="btn-embryo" aria-current="false" href="">Try again</a>
			</div>

		);
	}

}

export default ErrorSection;

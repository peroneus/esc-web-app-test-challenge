import React, { Fragment } from 'react';
import PropTypes from 'prop-types';

import { getEmbryoFrameOffset } from '../../utils';

import EmbryosChart from '../EmbryosChart';
import EmbryosImagesMilestones from '../EmbryosImagesMilestones';
import Legend from '../Legend';
import Timeline from '../Timeline';


export default function EmbryoDetail({ embryo, current, onReferenceLineChange }) {
	const frameOffset = getEmbryoFrameOffset(embryo);

	return (
		<Fragment>
			<EmbryosImagesMilestones
				embryo={embryo}
				offset={frameOffset}
				currentSelectedEmbryoReferenceLine={current}
			/>

			<EmbryosChart
				measurementCount={+embryo.image_count}
				offset={frameOffset}
				expansionData={embryo.expansion_data}
				activityData={embryo.activity_data}
				events={embryo.events}
				embryoReferenceLine={current}
				onReferenceLineChange={onReferenceLineChange}
			/>

			<Timeline
				count={+embryo.image_count + frameOffset}
				current={current || null}
			/>

			<Legend />
		</Fragment>
	);
}

EmbryoDetail.propTypes = {
	embryo: PropTypes.object.isRequired,
	current: PropTypes.number,
	onReferenceLineChange: PropTypes.func.isRequired,
};

EmbryoDetail.defaultProps = {
	current: 0,
};

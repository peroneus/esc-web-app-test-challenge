import React, { Component } from 'react';
import './styles.scss';

class InfoIcon extends Component {

	render() {

			return (
				<div className="info-icon-wrapper">
					<svg width="20px" height="20px" viewBox="0 0 20 20">
						<g id="Icons" stroke="none" strokeWidth="1" fill="none" fillRule="evenodd" transform="translate(-665.000000, -997.000000)">
							<g id="icon-info" transform="translate(665.000000, 997.000000)" fill="#A6A6A6" fillRule="nonzero">
								<path d="M9,5 L11,5 L11,7 L9,7 L9,5 Z M9,9 L11,9 L11,15 L9,15 L9,9 Z M9.99,0 C4.47,0 0,4.48 0,10 C0,15.52 4.47,20 9.99,20 C15.52,20 20,15.52 20,10 C20,4.48 15.52,0 9.99,0 Z M10,18 C5.58,18 2,14.42 2,10 C2,5.58 5.58,2 10,2 C14.42,2 18,5.58 18,10 C18,14.42 14.42,18 10,18 Z" id="Shape"></path>
							</g>
						</g>
					</svg>
				</div>
			);
	}

}

export default InfoIcon;

import React, { Component } from 'react';
import { ComposedChart, Line, Area, YAxis } from 'recharts';

import './styles.scss';

type TProps = {
  data: Array<any>,
  dataKey1: String,
  dataKey2: String,
  lineColor: String,
  areaColor: String,
  margin: any,
  width: number,
  height: number
};

class DataChart extends Component {

	props: TProps;

	shouldComponentUpdate(nextProps: TProps): boolean {

		if (nextProps.data !== this.props.data) {
			return true;
		}

		if (nextProps.dataKey1 !== this.props.dataKey1) {
			return true;
		}

		if (nextProps.dataKey2 !== this.props.dataKey2) {
			return true;
		}

		if (nextProps.lineColor !== this.props.lineColor) {
			return true;
		}

		if (nextProps.areaColor !== this.props.areaColor) {
			return true;
		}

		if (nextProps.width !== this.props.width) {
			return true;
		}

		if (nextProps.height !== this.props.height) {
			return true;
		}

		return false;

	}

	render() {

		return (

			<ComposedChart
				data={this.props.data}
				margin={this.props.margin}
				width={this.props.width}
				height={this.props.height}
			>

				<YAxis yAxisId={1} hide={true} domain={[0, 40000]} />
				<Area
					type="monotone"
					dataKey={this.props.dataKey2}
					yAxisId={1}
					isAnimationActive={false}
					stroke={this.props.areaColor}
					fill={this.props.areaColor}
				/>

				<YAxis yAxisId={2} hide={true} domain={[0, 1]} />
				<Line
					type="monotone"
					dataKey={this.props.dataKey1}
					yAxisId={2}
					stroke={this.props.lineColor}
					isAnimationActive={false}
					dot={false}
				/>


			</ComposedChart>

		);

	}

}

export default DataChart;

import React, { PureComponent, Fragment } from 'react';
import PropTypes from 'prop-types';
import ReactResizeDetector from 'react-resize-detector';
import range from 'lodash/range';

import { formatFrameTime } from '../../utils';
import './styles.scss';

const bigFrameInterval = 20;
const timelineBorder = 5;
const timeTextWidth = 34;


class Timeline extends PureComponent {

	state = {
		width: -1,
	};

	handleResize = (width) => {
		this.setState({ width });
	};

	getCurrentTimeTextAnchor() {
		const { count, current } = this.props;

		if (current <= timelineBorder) {
			return 'start';
		}

		if (current >= count - timelineBorder) {
			return 'end';
		}

		return 'middle';
	}

	render() {
		const { count, current } = this.props;
		const { width } = this.state;
		const xStep = width / count;
		const halfStep = xStep / 2;

		const currentXPosition = current !== null ? (current * xStep) + halfStep : null;

		return (
			<div className="timeline">
				<ReactResizeDetector handleWidth onResize={this.handleResize} />

				{width < 0 ? null : (
					<svg width={width} height="40">

						<line x1={halfStep - 0.5} y1={1} x2={width - halfStep + 0.5} y2={1} stroke="#7F7F7F" strokeWidth={2} />

						{range(count).map(i => {
							const x = (i * xStep) + halfStep;
							const isBig = (i % bigFrameInterval) === 0;
							const bigStyle = isBig ? {
								textAnchor: x <= timelineBorder ? 'start' : 'middle',
								opacity: (
									currentXPosition !== null &&
									currentXPosition >= x - timeTextWidth &&
									currentXPosition <= x + timeTextWidth
								) ? 0.2 : 1,
							} : null;

							return (
								<Fragment key={i}>
									<line x1={x} y1={0} x2={x} y2={isBig ? 20 : 10} stroke="#7F7F7F" strokeWidth={1} />
									{isBig ? (
										<text x={x} y={35} fill="#7F7F7F" fontSize={12} style={bigStyle}>
											{formatFrameTime(i)}
										</text>
									): null}
								</Fragment>
							);
						})}

						{current !== null ? (
							<text x={currentXPosition} y={35} fontSize={12} fill="red" style={{
								textAnchor: this.getCurrentTimeTextAnchor(),
							}}>
								{formatFrameTime(current)}
							</text>
						): null}
					</svg>
				)}
			</div>
		);
	}

}

Timeline.propTypes = {
	count: PropTypes.number.isRequired,
	current: PropTypes.number,
};

Timeline.defaultProps = {
	current: null,
};

export default Timeline;

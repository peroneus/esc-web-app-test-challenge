import React from 'react';
import './styles.scss';


export default function GraphIconThree() {
	return (
		<div className="arrow-down-graph-wrapper icon-graph-three-wrapper">
			<svg width="19px" height="13px" viewBox="0 0 19 13">
				<g id="Icons" stroke="none" strokeWidth="1" fill="none" fillRule="evenodd" transform="translate(-548.000000, -896.000000)">
					<polyline id="icon-graph3" stroke="#979797" points="548.875083 909 548.875083 904.502443 556.263342 904.502443 556.263342 900.935877 558.735777 900.935877 558.735777 897 566.124917 897" />
				</g>
			</svg>
		</div>
	);
}

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import { actions as learningActions } from '../../reducers/embryos_learning_reducer';
import { actions as trainingActions } from '../../reducers/embryos_training_reducer';

import TripleDownArrow from '../../components/TripleDownArrow';

import './styles.scss';

class StagesList extends Component {

	onStageItemClick(stage) {

		// Learning Stages Section

		if(this.props.type === "learning_stages") {

			const clustersByStage = this.props.clustersByStage;
			const stageObject = clustersByStage[stage];

			this.props.dispatch(learningActions.uiSelectStage(stageObject));

		}

		// Training Question Section

		else if(this.props.type === "training") {

			const currentTrainingQuestionResult = this.props.currentTrainingQuestionResult;

			if(currentTrainingQuestionResult === undefined || Object.keys(currentTrainingQuestionResult).length === 0) {

				const currentSelectedStage = this.getStageObjectByValue(stage);
				this.props.dispatch(trainingActions.uiSelectOptionAtTrainingQuestion(currentSelectedStage));

			}

		}

		// Test Question Section

		else if(this.props.type === "test") {

			const testQuestionId = this.props.testQuestionId;
			const testQuestionIndex = this.props.testQuestionIndex;
			const currentTrainingTestResult = this.props.currentTrainingTestResult;

			if(currentTrainingTestResult === undefined || Object.keys(currentTrainingTestResult).length === 0) {

				const currentSelectedStage = this.getStageObjectByValue(stage);
				this.props.dispatch(trainingActions.uiSelectOptionAtTestQuestion(testQuestionId, testQuestionIndex, currentSelectedStage));

			}

		}

	}

	getStageObjectByValue(value) {

		const options = this.props.options;
		let result = null;

		options.forEach((item) => {

			if(item.value === value)
				result = item;

		});

		return result;

	}

	findAnsweredQuestionByQuestionId(answeredQuestions) {

		let result = null;
		const questionId = this.props.testQuestionId;

		answeredQuestions.forEach((question, index) => {

			if(question.questionId === questionId)
				result = question;


		});

		return result;

	}

	render() {

		const type = this.props.type;

		const stagesList = this.props.stagesList;
		const options = this.props.options;

		const currentSelectedStageValue = this.props.currentSelectedStageValue;
		const currentTrainingQuestionResult = this.props.currentTrainingQuestionResult;
		const isCorrectSelectedOption = this.props.isCorrectSelectedOption;

		return (

			stagesList.map(function (value, index) {

				let stageItemClassName = (value === currentSelectedStageValue) ? "designation-num active" : "designation-num";
				let responseClassName = "";

				let valueLength = (value.length < 3) ? "short" : "long";

				// For Training: Question Result

				if(type === "training") {

					if(currentTrainingQuestionResult !== undefined && Object.keys(currentTrainingQuestionResult).length > 0) {

						if(value === currentSelectedStageValue) {

							responseClassName = (isCorrectSelectedOption) ? " correct" : " incorrect";

						} else {

							if(!isCorrectSelectedOption) {

								if(value === currentTrainingQuestionResult.correct)
									responseClassName= " correct";

							} else {

								responseClassName = "";

							}

						}

					}

				}

				// For Test: Question Result

				else if (type === "test") {

					const currentTrainingTestResult = this.props.currentTrainingTestResult;

					if(currentTrainingTestResult !== undefined && Object.keys(currentTrainingTestResult).length > 0) {

						if(value === currentSelectedStageValue) {

							const isSuccess = currentTrainingTestResult.answeredQuestions[this.props.testQuestionIndex].success;
							responseClassName = (isSuccess) ? " correct" : " incorrect";

						} else {

							if(!isCorrectSelectedOption) {

								const answeredQuestions = currentTrainingTestResult["answeredQuestions"];
								const answeredQuestion =  this.findAnsweredQuestionByQuestionId(answeredQuestions);

								if(value === answeredQuestion.correct)
									responseClassName= " correct";

							} else {

								responseClassName = "";

							}

						}

					}



				}

				stageItemClassName = stageItemClassName + responseClassName;

				// Get Description

				let description = null;

				if (type === "training")
					description = <p>{options[index].shortName}</p>
				else if(type === "test")
					description = <p>{options[index].description}</p>

				// Get Triple Arrow

				let tripleArrow = null;

				if(type === "training")
					tripleArrow = <div className="tripple-arrow-upper-wrapper"><TripleDownArrow /></div>;

				return  (

					<div key={value} className="designation-num-container">

						<div className={stageItemClassName} data-length={valueLength} onClick={this.onStageItemClick.bind(this, value)}>
							{value}
							{type === 'learning_stages' && <div className="img">
								<img src={this.props.clustersByStage[value].embryo.last_image_url} alt={`Cluster ${value} embryo`} />
							</div>}
						</div>
						{description}
						{tripleArrow}

					</div>

				);

			}, this)

		);

	}

}

StagesList.propTypes = {

	type: PropTypes.string,
	clustersByStage: PropTypes.object,

	stagesList: PropTypes.array,
	options: PropTypes.array,

	currentSelectedStageValue: PropTypes.string,

	isCorrectSelectedOption: PropTypes.bool,
	currentTrainingQuestionResult: PropTypes.object,

	currentTrainingTestResult: PropTypes.object,

	testQuestionId: PropTypes.number,
	testQuestionIndex: PropTypes.number,

	dispatch: PropTypes.func.isRequired

};

export default connect()(StagesList);

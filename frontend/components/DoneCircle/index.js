import React, { Component } from 'react';

class DoneCircle extends Component {

	render() {

		return (
			<div className="done-circle-wrapper">
				<svg width="16px" height="16px" viewBox="0 0 16 16" >
					<g id="Icons" stroke="none" strokeWidth="1" fill="none" fillRule="evenodd" transform="translate(-668.000000, -1111.000000)">
						<g id="progressbar-done" transform="translate(668.000000, 1111.000000)">
							<g id="done-mark-copy">
								<circle id="Oval-9" fill="#61B945" cx="8" cy="8" r="8"></circle>
								<polyline id="Path-6-Copy" stroke="#FFFFFF" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round" points="4.66666667 8.14748005 7.0240297 10.2222222 11.3333333 5.77777778"></polyline>
							</g>
						</g>
					</g>
				</svg>
			</div>
		);
	}

}

export default DoneCircle;

<?php
declare(strict_types = 1);

namespace Netvor\Embryo\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;


/**
 * Fix Early Blast short name
 */
class Version20180220092209 extends AbstractMigration
{
	public function up(Schema $schema)
	{
		$this->addSql('UPDATE `cluster` SET `short_name` = \'Early Blast\' WHERE `id` = 24');
	}


	public function down(Schema $schema)
	{
		$this->addSql('UPDATE `cluster` SET `short_name` = \'EBlast\' WHERE `id` = 24;');
	}
}

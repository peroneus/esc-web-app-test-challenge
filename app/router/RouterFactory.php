<?php
declare(strict_types=1);

namespace Netvor\Embryo\Router;

use Nette;
use Nette\Application\Routers\Route;
use Nette\Application\Routers\RouteList;


class RouterFactory
{
	use Nette\StaticClass;

	public static function createRouter(): RouteList
	{
		$router = new RouteList;
		$router[] = new Route('<module api>/<presenter>[/<id>]');
		$router[] = new Route('<presenter app|tutorial>[/<path .+>]', [
			'action' => 'default',
			'path' => [
				Route::VALUE => null,
				Route::FILTER_IN => null,
				Route::FILTER_OUT => null,
			],
		]);
		$router[] = new Route('<presenter>[/<action>][/<id>]', 'Homepage:default');
		return $router;
	}
}

<?php
declare(strict_types=1);

namespace Netvor\Embryo\Model;

use Kdyby\Doctrine\EntityManager;
use Kdyby\Doctrine\EntityRepository;
use Nette;
use Netvor\Embryo\Mails\MailService;


class UserPasswordService
{
	use Nette\SmartObject;

	/** @var int */
	public $codeLength = 10;

	/** @var string */
	public $codeValidDuration = '30 minutes';

	/** @var MailService */
	private $mailService;

	/** @var EntityManager */
	private $entityManager;

	/** @var EntityRepository */
	private $repository;


	public function __construct(MailService $mailService, EntityManager $entityManager)
	{
		$this->mailService = $mailService;
		$this->entityManager = $entityManager;

		/** @var EntityRepository $passwordCodeRepository */
		$passwordCodeRepository = $this->entityManager->getRepository(Entities\UserPasswordCode::class);
		$this->repository = $passwordCodeRepository;
	}


	public function getCode(Entities\User $user, string $code): ?Entities\UserPasswordCode
	{
		$codes = array_filter(
			$this->repository->findBy([
				'user' => $user,
				'created >=' => new Nette\Utils\DateTime('now - ' . $this->codeValidDuration),
			]),
			function (Entities\UserPasswordCode $entity) use ($code) {
				return $entity->verifyCode($code);
			}
		);
		return count($codes) > 0 ? reset($codes) : null;
	}


	public function createCode(Entities\User $user): string
	{
		$code = Nette\Utils\Random::generate($this->codeLength);

		$this->entityManager->persist(new Entities\UserPasswordCode($user, $code));
		$this->entityManager->flush();

		$this->mailService->send('resetPassword', $user->getEmail(), [
			'user' => $user,
			'code' => $code,
		]);

		return $code;
	}


	public function resetPassword(Entities\UserPasswordCode $code, string $password): void
	{
		$code->getUser()->setPassword($password);
		$this->entityManager->remove($code);
		$this->entityManager->flush();
	}
}

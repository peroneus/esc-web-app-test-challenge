<?php
declare(strict_types=1);

namespace Netvor\Embryo\Model;

use Nette;
use Nette\Caching\Cache;


class CacheCleanService
{
	/** @var Cache[] */
	private $toClean;

	/** @var int[] */
	private $userIds;


	/**
	 * @param Cache[] $toClean
	 * @param int[] $userIds
	 */
	public function __construct(array $toClean, array $userIds)
	{
		if (!Nette\Utils\Validators::everyIs($toClean, Cache::class)) {
			throw new \InvalidArgumentException;
		}
		$this->toClean = $toClean;

		if (!Nette\Utils\Validators::everyIs($userIds, 'int')) {
			throw new \InvalidArgumentException;
		}
		$this->userIds = $userIds;
	}


	public function isAuthorized(Nette\Security\User $user): bool
	{
		return $user->isLoggedIn() && in_array($user->getId(), $this->userIds, true);
	}


	public function clean(): void
	{
		foreach ($this->toClean as $cache) {
			$cache->clean([Cache::ALL => true]);
		}
	}
}

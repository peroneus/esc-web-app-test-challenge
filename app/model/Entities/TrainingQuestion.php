<?php
declare(strict_types=1);

namespace Netvor\Embryo\Model\Entities;

use Doctrine\ORM\Mapping as ORM;
use Nette;


/**
 * @ORM\Entity
 * @property-read ?Nette\Utils\DateTime $answeredAt
 */
class TrainingQuestion extends BaseQuestion
{
	use Nette\SmartObject;

	/**
	 * @var User
	 * @ORM\ManyToOne(targetEntity="User")
	 * @ORM\JoinColumn(nullable=false, onDelete="CASCADE")
	 */
	private $user;

	/**
	 * @ORM\Column
	 * @var string $level one of the User::LEVELS
	 */
	private $level;

	/**
	 * @ORM\Column(type="datetime", nullable=true)
	 * @var ?\DateTime
	 */
	private $answeredAt;


	public function __construct(User $user, int $embryoId, string $level)
	{
		parent::__construct($embryoId);
		$this->user = $user;

		if (
			!in_array($level, self::LEVELS, true) ||
			!in_array($this->user->getLevel(), self::USER_LEVELS[$level], true)
		) {
			throw new \InvalidArgumentException;
		}
		$this->level = $level;
	}


	public function getUser(): User
	{
		return $this->user;
	}


	public function getLevel(): string
	{
		return $this->level;
	}


	public function getAnsweredAt(): ?Nette\Utils\DateTime
	{
		return $this->answeredAt !== null ? Nette\Utils\DateTime::from($this->answeredAt) : null;
	}


	public function answer(string $answer, string $correctClassification): self
	{
		parent::answer($answer, $correctClassification);
		$this->answeredAt = new Nette\Utils\DateTime;
		return $this;
	}
}

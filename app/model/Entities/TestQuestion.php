<?php
declare(strict_types=1);

namespace Netvor\Embryo\Model\Entities;

use Doctrine\ORM\Mapping as ORM;
use Nette;


/**
 * @ORM\Entity
 * @property-read Test $test
 */
class TestQuestion extends BaseQuestion
{
	use Nette\SmartObject;

	/**
	 * @var Test
	 * @ORM\ManyToOne(targetEntity="Test", inversedBy="questions")
	 * @ORM\JoinColumn(nullable=false, onDelete="CASCADE")
	 */
	private $test;

	/**
	 * @ORM\Column(type="smallint", name="`order`")
	 * @var int
	 */
	private $order;


	public function __construct(Test $test, int $embryoId, int $order)
	{
		parent::__construct($embryoId);
		$this->test = $test;
		$this->order = $order;
	}


	public function getTest(): Test
	{
		return $this->test;
	}


	public function getUser(): User
	{
		return $this->test->getUser();
	}


	public function getLevel(): string
	{
		return $this->test->getLevel();
	}
}

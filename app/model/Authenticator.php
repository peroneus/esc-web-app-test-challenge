<?php
declare(strict_types=1);

namespace Netvor\Embryo\Model;

use Nette;
use Nette\Security;


class Authenticator implements Security\IAuthenticator
{
	use Nette\SmartObject;

	/** @var UserAuthService */
	private $model;


	public function __construct(UserAuthService $model)
	{
		$this->model = $model;
	}


	public function authenticate(array $credentials): Security\IIdentity
	{
		[$email, $password] = $credentials;

		$user = $this->model->getByEmail($email);

		if ($user === null) {
			throw new Security\AuthenticationException('The e-mail is incorrect.', self::IDENTITY_NOT_FOUND);
		}

		if (!$user->verifyPassword($password)) {
			throw new Security\AuthenticationException('The password is incorrect.', self::INVALID_CREDENTIAL);
		}
		return new Security\Identity($user->getId());
	}
}

<?php
declare(strict_types=1);

namespace Netvor\Embryo\Console;

use Nette;
use Nette\Utils\Finder;
use Nette\Utils\Image;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;


class ImportImagesCommand extends Command
{
	private const IMAGE_MASK = 'EM_*.jpg';

	private const EMBRYO_WIDTH = 250;

	private const EMBRYO_HEIGHT = 250;

	/** @var string */
	private $targetDirectory;


	public function __construct(string $targetDirectory)
	{
		parent::__construct();
		$this->targetDirectory = $targetDirectory;
	}


	protected function configure(): void
	{
		$this->setName('images:import');
		$this->setDescription('Import time lapse images.');
		$this->addArgument('directory', InputArgument::REQUIRED, 'Directory to search for images');
		$this->addArgument('quality', InputArgument::REQUIRED, 'Image quality (1-100)');
	}


	protected function execute(InputInterface $input, OutputInterface $output): int
	{
		$directory = (string) $input->getArgument('directory');

		if (!is_dir($this->targetDirectory) || !is_writable($this->targetDirectory)) {
			$output->writeln('<error>Error: target directory is not writable</error>');
			return 1;
		}

		$quality = $input->getArgument('quality');
		if (!Nette\Utils\Validators::is($quality, 'numericint:1..100')) {
			$output->writeln('<error>Quality must be a number between 1 and 100.</error>');
			return 1;
		}


		foreach (Finder::findFiles('embryos')->from($directory) as $embryosFile) {
			$embryosDir = dirname((string) $embryosFile);
			$images = iterator_to_array(Finder::findFiles(self::IMAGE_MASK)->in($embryosDir));
			usort($images, function (\SplFileInfo $a, \SplFileInfo $b) {
				return $a->getBasename() <=> $b->getBasename();
			});
			$imageCount = count($images);

			if (!($imageCount > 0)) {
				$output->writeln('Warning: No images for ' . (string) $embryosFile);
				continue;
			}

			$fHandle = fopen((string) $embryosFile, 'r');
			if (!$fHandle) {
				$output->writeln('Warning: Error opening ' . (string) $embryosFile);
				continue;
			}

			$output->writeln('Processing embryos in file ' . (string) $embryosFile . '...');

			while (($line = fgetcsv($fHandle)) !== false) {
				if (count($line) !== 3) {
					$output->writeln('Warning: wrong column count in file ' . (string) $embryosFile);
					continue;
				}

				[$id, $col, $row] = $line;

				if ($id < 0) {
					$output->writeln('Warning: embryo ID < 0 in file ' . (string) $embryosFile);
					continue;
				}

				$output->writeln('Processing embryo ' . $id . '...');

				$targetImage = Image::fromBlank(self::EMBRYO_WIDTH * $imageCount, self::EMBRYO_HEIGHT);
				for ($i = 0; $i < $imageCount - 1; $i++) {
					$source = Image::fromFile((string) $images[$i]);
					self::copyEmbryoImage($targetImage, $i, $source, $col, $row);
					imagedestroy($source->getImageResource());
				}

				$source = Image::fromFile((string) $images[$imageCount - 1]);
				$lastImage = Image::fromBlank(self::EMBRYO_WIDTH, self::EMBRYO_HEIGHT);
				self::copyEmbryoImage($targetImage, $imageCount - 1, $source, $col, $row);
				self::copyEmbryoImage($lastImage, 0, $source, $col, $row);
				imagedestroy($source->getImageResource());

				$targetImage->save($this->targetDirectory . DIRECTORY_SEPARATOR . $id . '.jpg', (int) $quality);
				$lastImage->save($this->targetDirectory . DIRECTORY_SEPARATOR . $id . '_last.jpg', (int) $quality);
				imagedestroy($targetImage->getImageResource());
				imagedestroy($lastImage->getImageResource());
			}

			$output->writeln('Done.');
		}
		return 0;
	}


	private static function copyEmbryoImage(Image $targetImage, $imageNumber, Image $source, $col, $row): void
	{
		$targetImage->copy(
			$source,
			$imageNumber * self::EMBRYO_WIDTH,
			0,
			$col * self::EMBRYO_WIDTH,
			$row * self::EMBRYO_HEIGHT,
			self::EMBRYO_WIDTH,
			self::EMBRYO_HEIGHT
		);
	}
}

<?php
declare(strict_types=1);

namespace Netvor\Embryo\Presenters;

use Netvor\Embryo\Model\Entities\User;
use Netvor\Embryo\Model\UserService;


abstract class BaseLoggedInPresenter extends BasePresenter
{
	/** @var UserService */
	private $userModel;

	private $userChecked = false;


	public function injectUserModel(UserService $userModel)
	{
		$this->userModel = $userModel;
	}


	protected function startup()
	{
		parent::startup();
		if (!$this->getUser()->isLoggedIn()) {
			$this->flashMessage('You have to log in to view this page.', 'warning');
			$this->redirect(':Login:', ['backlink' => $this->storeRequest()]);
		}
		$this->userChecked = true;
	}


	protected function beforeRender()
	{
		parent::beforeRender();
		$this->template->userIdentity = $this->getUserIdentity();
	}


	protected function getUserIdentity(): User
	{
		if (!$this->userChecked) {
			throw new \RuntimeException('Cannot get user identity before the user was checked.');
		}

		$user = $this->userModel->get($this->getUser()->getId());

		if ($user === null) {
			$this->error();
		}

		return $user;
	}
}

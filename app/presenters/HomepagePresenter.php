<?php
declare(strict_types=1);

namespace Netvor\Embryo\Presenters;

use Netvor\Embryo\Components\IContactControlFactory;


class HomepagePresenter extends BasePresenter
{
	/**
	 * @var IContactControlFactory
	 * @inject
	 */
	public $contactControlFactory;


	public function renderDefault()
	{
		$this->getSession()->start();
	}


	protected function createComponentContactControl()
	{
		return $this->contactControlFactory->create();
	}
}

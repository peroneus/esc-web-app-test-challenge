<?php
declare(strict_types=1);

namespace Netvor\Embryo\ApiModule\Presenters;

use Nette;
use Nette\Application;
use Nette\Utils\Arrays;
use Netvor;
use Netvor\Embryo\ApiModule\Model\EmbryoService;
use Netvor\Embryo\Model\Entities\Cluster;
use Netvor\Embryo\Model\Entities\TrainingQuestion;
use Netvor\Embryo\Model\Entities\User;
use Netvor\Embryo\Model\TrainingService;
use Tracy\ILogger;


class TrainingPresenter extends BaseLoggedInPresenter
{

	/** @var TrainingService */
	private $model;

	/** @var EmbryoService */
	private $embryoModel;

	/** @var ILogger */
	private $logger;


	public function __construct(TrainingService $model, EmbryoService $embryoModel, ILogger $logger)
	{
		$this->model = $model;
		$this->embryoModel = $embryoModel;
		$this->logger = $logger;
	}


	public function getAll(Application\Request $request): Application\IResponse
	{
		$user = $this->getUserIdentity();
		$levels = array_filter(TrainingQuestion::LEVELS, function ($level) use ($user) {
			return in_array($user->getLevel(), TrainingQuestion::USER_LEVELS[$level], true);
		});

		/** @var ?string $level */
		$level = Netvor\Embryo\Utils\Validator::validateField($request->getParameters(), 'level', [$levels], $errors, false);
		if (!empty($errors)) {
			throw $this->error(implode("\n", $errors), 400);
		}

		return $this->json([
			'currentLevel' => $user->getLevel(),
			'nextLevel' => $nextLevel = $user->getNextLevel(),
			'currentTrainedEmbryos' => $this->model->getCurrentCount($user, $level),
			'totalEmbryos' => TrainingService::MIN_QUESTION_COUNT,
			'accuracyPercentage' => $this->model->getAccuracy($user, $level),
			'minAccuracyPercentage' => TrainingService::MIN_ACCURACY,
		]);
	}


	public function post(array $data, Application\Request $request): Application\IResponse
	{
		$user = $this->getUserIdentity();
		$levels = array_filter(TrainingQuestion::LEVELS, function ($level) use ($user) {
			return in_array($user->getLevel(), TrainingQuestion::USER_LEVELS[$level], true);
		});

		/** @var ?string $level */
		$level = Netvor\Embryo\Utils\Validator::validateField($data, 'level', [$levels], $errors, false);
		if (!empty($errors)) {
			throw $this->error(implode("\n", $errors), 400);
		}

		$clusters = $this->embryoModel->getClusters();
		$clustersByCatiId = Arrays::associate(Arrays::map($clusters, function (Cluster $cluster) {
			return [
				'catiId' => $cluster->getCatiId(),
				'cluster' => $cluster,
			];
		}), 'catiId=cluster');

		$question = $this->model->nextQuestion($clusters, $user, $level ?? $levels[count($levels) - 1]);
		$embryo = $this->embryoModel->getEmbryoById($question->getEmbryoId());
		if ($embryo === null) {
			$message = sprintf('Embryo #%d not found.', $question->getEmbryoId());
			$this->logger->log(new \RuntimeException($message), ILogger::EXCEPTION);
			throw $this->error($message, 404);
		}
		$annotations = $this->embryoModel->getAnnotations([$embryo['EmbryoId']]);

		if (!isset($clustersByCatiId[$embryo['CurveClass']])) {
			$message = sprintf('Cluster #%d not found.', $embryo['CurveClass']);
			$this->logger->log(new \RuntimeException($message), ILogger::EXCEPTION);
			throw $this->error($message, 404);
		}
		$cluster = $clustersByCatiId[$embryo['CurveClass']];
		$options = TrainingQuestion::getOptionsByCluster($clusters, $cluster, $level ?? $levels[count($levels) - 1]);

		return $this->json([
			'id' => $question->getId(),
			'level' => $question->getLevel(),
			'embryo' => $this->embryoModel->formatEmbryo($embryo, $annotations),
			'options' => $this->formatOptions($options, $level ?? $levels[count($levels) - 1]),
		]);
	}


	public function put(string $id, array $data, Application\Request $request): Application\IResponse
	{
		try {
			Nette\Utils\Validators::assert($id, 'numericint:1..', 'id');
		} catch (Nette\Utils\AssertionException $e) {
			throw $this->error($e->getMessage(), 400);
		}

		$user = $this->getUserIdentity();
		$question = $this->model->getQuestion($user, (int) $id);
		if ($question === null) {
			throw $this->error('Question not found.', 404);
		}

		$clusters = $this->embryoModel->getClusters();
		$clustersByCatiId = Arrays::associate(Arrays::map($clusters, function (Cluster $cluster) {
			return [
				'catiId' => $cluster->getCatiId(),
				'cluster' => $cluster,
			];
		}), 'catiId=cluster');

		$embryo = $this->embryoModel->getEmbryoById($question->getEmbryoId());
		if ($embryo === null) {
			$message = sprintf('Embryo #%d not found.', $question->getEmbryoId());
			$this->logger->log(new \RuntimeException($message), ILogger::EXCEPTION);
			throw $this->error($message, 404);
		}
		if (!isset($clustersByCatiId[$embryo['CurveClass']])) {
			$message = sprintf('Cluster #%d not found.', $embryo['CurveClass']);
			$this->logger->log(new \RuntimeException($message), ILogger::EXCEPTION);
			throw $this->error($message, 404);
		}

		$cluster = $clustersByCatiId[$embryo['CurveClass']];
		$options = TrainingQuestion::getOptionsByCluster($clusters, $cluster, $level = $question->getLevel());
		$answerOptions = Arrays::map($options, function (Cluster $option) use ($level) {
			return substr($option->getClassification(), 0, TrainingQuestion::CLASSIFICATION_LENGTHS[$level]);
		});

		/** @var string $answer */
		$answer = Netvor\Embryo\Utils\Validator::validateField($data, 'answer', [$answerOptions], $errors);
		if (!empty($errors)) {
			throw $this->error(implode("\n", $errors), 400);
		}

		$this->model->answerQuestion($question, $cluster, $answer);

		return $this->json([
			'questionId' => $question->getId(),
			'success' => $question->isCorrect(),
			'correct' => substr($cluster->getClassification(), 0, TrainingQuestion::CLASSIFICATION_LENGTHS[$level]),
			'currentTrainedEmbryos' => $this->model->getCurrentCount($user, $level),
			'accuracyPercentage' => $this->model->getAccuracy($user, $level),
		]);
	}


	private function formatOptions(array $options, string $level): array
	{
		$descriptions = $this->embryoModel->getLearningDescriptions();
		$embryos = $this->embryoModel->getRepresentativeEmbryos();
		$annotations = $this->embryoModel->getAnnotations(array_column($embryos, 'EmbryoId'));

		return Arrays::map($options, function (Cluster $cluster) use ($level, $descriptions, $embryos, $annotations) {
			$description = $this->getClusterDescription($cluster, $level, $descriptions);
			$embryo = $embryos[$cluster->getClassification()] ?? null;

			return [
				'value' => substr($cluster->getClassification(), 0, TrainingQuestion::CLASSIFICATION_LENGTHS[$level]),
				'shortName' => $description['shortName'] ?? ($level === User::LEVEL_BEGINNER ?
					$cluster->getDevelopmentPhase() :
					$cluster->getShortName()
				),
				'name' => $description['name'] ?? $cluster->getName(),
				'description' => $description['description'] ?? $cluster->getDescription(),
				'embryo' => $embryo !== null ? $this->embryoModel->formatEmbryo($embryo, $annotations) : null,
			];
		});
	}


	private function getClusterDescription(Cluster $cluster, string $level, array $descriptions): ?array
	{
		static $childrenKeys = [
			User::LEVEL_BEGINNER => 'byStage',
			User::LEVEL_INTERMEDIATE => 'byQuality',
			User::LEVEL_EXPERT => 'byEvents',
		];

		$clusterKeys = Arrays::map(TrainingQuestion::CLASSIFICATION_LENGTHS, function (int $length) use ($cluster) {
			return substr($cluster->getClassification(), 0, $length);
		});

		$current = [$childrenKeys[User::LEVEL_BEGINNER] => $descriptions];
		foreach (TrainingQuestion::LEVELS as $currentLevel) {
			$childrenKey = $childrenKeys[$currentLevel] ?? null;
			$clusterKey = $clusterKeys[$currentLevel] ?? null;
			if (!isset($childrenKey, $clusterKey, $current[$childrenKey][$clusterKey])) {
				return null;
			}
			$current = $current[$childrenKey][$clusterKey];
			if ($currentLevel === $level) {
				return $current;
			}
		}

		return null;
	}
}

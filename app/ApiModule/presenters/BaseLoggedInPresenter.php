<?php
declare(strict_types=1);

namespace Netvor\Embryo\ApiModule\Presenters;

use Nette\Application;
use Nette\Security;
use Netvor\Embryo\Model\Entities\User;
use Netvor\Embryo\Model\UserService;


abstract class BaseLoggedInPresenter extends BasePresenter
{
	/** @var Security\User */
	private $user;

	/** @var UserService */
	private $userModel;

	private $userChecked = false;


	public function injectUserServices(Security\User $user, UserService $userModel)
	{
		$this->user = $user;
		$this->userModel = $userModel;
	}


	public function run(Application\Request $request): Application\IResponse
	{
		if (!$this->user->isLoggedIn()) {
			return $this->errorJson('You need to be logged in to access this page.', 401);
		}
		$this->userChecked = true;
		return parent::run($request);
	}


	protected function getUser(): Security\User
	{
		return $this->user;
	}


	protected function getUserIdentity(): User
	{
		if (!$this->userChecked) {
			throw new \RuntimeException('Cannot get user identity before the user was checked.');
		}

		$user = $this->userModel->get($this->getUser()->getId());

		if ($user === null) {
			throw $this->error('User not found.');
		}

		return $user;
	}
}
